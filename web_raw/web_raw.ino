/*********
  Rui Santos
  Complete project details at https://RandomNerdTutorials.com/esp32-async-web-server-espasyncwebserver-library/
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

// Import required libraries
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "index_html.h"
#include <EEPROM.h>

#include <Arduino_JSON.h>
// Replace with your network credentials

const char* ssidAP     = "ESP32";
const char* passwordAP = "";


// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

String  Essid   = "";                 //EEPROM Network SSID
String  Epass   = "";                 //EEPROM Network Password
String  Etoken = "";                 //EEPROM Network token
int  EtempThreshold1 = 0;
int  EtempThreshold2 = 0;
int  EhumiThreshold1 = 0;
int  EhumiThreshold2 = 0;
int  EdustThreshold1 = 0;
int  EdustThreshold2 = 0;

/*
 * @brif : Hàm đọc data từ client 
*/
void getDataFromClient(AsyncWebServerRequest *request, uint8_t *data, size_t len, size_t index, size_t total) {
  Serial.print("get data : ");
  Serial.println((char *)data);
  JSONVar myObject = JSON.parse((char *)data);
  if(myObject.hasOwnProperty("ssid"))
    Essid = (const char*) myObject["ssid"];
  if(myObject.hasOwnProperty("pass"))
    Epass = (const char*)myObject["pass"] ;
  if(myObject.hasOwnProperty("token"))
    Etoken = (const char*)myObject["token"];
  if(myObject.hasOwnProperty("tempThreshold1"))
    EtempThreshold1 = (int) myObject["tempThreshold1"];
  if(myObject.hasOwnProperty("tempThreshold2")) 
    EtempThreshold2 = (int) myObject["tempThreshold2"];
  if(myObject.hasOwnProperty("humiThreshold1")) 
    EhumiThreshold1 = (int) myObject["humiThreshold1"];
  if(myObject.hasOwnProperty("humiThreshold2")) 
    EhumiThreshold2 = (int) myObject["humiThreshold2"];
  if(myObject.hasOwnProperty("dustThreshold1")) 
    EdustThreshold1 = (int) myObject["dustThreshold1"];
  if(myObject.hasOwnProperty("dustThreshold2")) 
    EdustThreshold2 = (int) myObject["dustThreshold2"];
  writeEEPROM();
}

void printValueSetup() {
    Serial.print("ssid = ");
    Serial.println(Essid);
    Serial.print("pass = ");
    Serial.println(Epass);
    Serial.print("token = ");
    Serial.println(Etoken);
    Serial.print("tempThreshold1 = ");
    Serial.println(EtempThreshold1);
    Serial.print("tempThreshold2 = ");
    Serial.println(EtempThreshold2);
    Serial.print("humiThreshold1 = ");
    Serial.println(EhumiThreshold1);
    Serial.print("humiThreshold2 = ");
    Serial.println(EhumiThreshold2);
    Serial.print("dustThreshold1 = ");
    Serial.println(EdustThreshold1);
    Serial.print("dustThreshold2 = ");
    Serial.println(EdustThreshold2);
}

String getJsonData() {
  JSONVar myObject;
  myObject["ssid"] = Essid;
  myObject["pass"] = Epass;
  myObject["token"] = Etoken;
  myObject["tempThreshold1"] = EtempThreshold1;
  myObject["tempThreshold2"] = EtempThreshold2;
  myObject["humiThreshold1"] = EhumiThreshold1;
  myObject["humiThreshold2"] = EhumiThreshold2;
  myObject["dustThreshold1"] = EdustThreshold1;
  myObject["dustThreshold2"] = EdustThreshold2;

  String jsonData = JSON.stringify(myObject);
  return jsonData;
}

//--------------------------- Read Eeprom  ------------------------------------
void readEEPROM() {
    for (int i = 0; i < 32; ++i)       //Reading SSID
        Essid += char(EEPROM.read(i)); 
    for (int i = 32; i < 64; ++i)      //Reading Password
        Epass += char(EEPROM.read(i)); 
    for (int i = 64; i < 96; ++i)      //Reading Password
        Etoken += char(EEPROM.read(i)); 

    EtempThreshold1 = EEPROM.read(200);
    EtempThreshold2 = EEPROM.read(201);

    EhumiThreshold1 = EEPROM.read(202);
    EhumiThreshold2 = EEPROM.read(203);

    EdustThreshold1 = EEPROM.read(204) * 100 + EEPROM.read(205);
    EdustThreshold2 = EEPROM.read(206) * 100 + EEPROM.read(207);  

    printValueSetup();
}

// --------------------------- Clear Eeprom ---------------------------
void clearEeprom() {
    Serial.println("Clearing Eeprom");
    for (int i = 0; i < 250; ++i) 
      EEPROM.write(i, 0);
}

void writeEEPROM() {
    for (int i = 0; i < Essid.length(); ++i)
          EEPROM.write(i, Essid[i]);  
    for (int i = 0; i < Epass.length(); ++i)
          EEPROM.write(32+i, Epass[i]);
    for (int i = 0; i < Etoken.length(); ++i)
          EEPROM.write(64+i, Etoken[i]);

    EEPROM.write(200, EtempThreshold1);          // lưu ngưỡng nhiệt độ 1
    EEPROM.write(201, EtempThreshold2);          // lưu ngưỡng nhiệt độ 2

    EEPROM.write(202, EhumiThreshold1);          // lưu ngưỡng độ ẩm 1
    EEPROM.write(203, EhumiThreshold2);          // lưu ngưỡng độ ẩm 2

    EEPROM.write(204, EdustThreshold1 / 100);      // lưu hàng nghìn + trăm bụi 1
    EEPROM.write(205, EdustThreshold1 % 100);      // lưu hàng chục + đơn vị bụi 1

    EEPROM.write(206, EdustThreshold2 / 100);      // lưu hàng nghìn + trăm bụi 2
    EEPROM.write(207, EdustThreshold2 % 100);      // lưu hàng chục + đơn vị bụi 2
    
    EEPROM.commit();

    Serial.println("write eeprom");
    delay(500);
}

void setup(){
  // Serial port for debugging purposes
  Serial.begin(115200);

  EEPROM.begin(512);
  // Đọc data setup từ eeprom
  readEEPROM();

  // Khởi tạo Wifi AP Mode, vui lòng kết nối wifi ESP32, truy cập 192.168.4.1
  WiFi.softAP(ssidAP, passwordAP);  

  // Gửi trang HTML khi client truy cập 192.168.4.1
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html);
  });

  // Gửi data ban đầu đến client
  server.on("/data_before", HTTP_GET, [](AsyncWebServerRequest *request){
    String json = getJsonData();
    request->send(200, "application/json", json);
  });

  // Get data ban đầu từ client
  server.on("/post_data", HTTP_POST, [](AsyncWebServerRequest *request){
    request->send(200, "text/plain", "SUCCESS");
  }, NULL, getDataFromClient);

  // Start server
  server.begin();
}

void loop() {

}