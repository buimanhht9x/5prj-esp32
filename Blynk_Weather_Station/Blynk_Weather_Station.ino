#define BLYNK_PRINT Serial
// You should get Auth Token in the Blynk App.
#define BLYNK_TEMPLATE_ID           "BANLINHKIEN"
#define BLYNK_TEMPLATE_NAME         "BANLINHKIEN"
char BLYNK_AUTH_TOKEN[32]   =   "";

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <LiquidCrystal.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <EEPROM.h>
#include "config.h"
#include <SimpleKalmanFilter.h>
#include "stringNotifications.h"

// --------------------------- Khai báo cho OLED 1.3 --------------------------------------
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#define i2c_Address 0x3C   //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's
#define SCREEN_WIDTH 128   // OLED display width, in pixels
#define SCREEN_HEIGHT 64   // OLED display height, in pixels
#define OLED_RESET -1      //   QT-PY / XIAO
Adafruit_SH1106G oled = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define OLED_SDA      21
#define OLED_SCL      22
// --------------------- Cảm biến DHT11 ---------------------
#include "DHT.h"
#define DHT11_PIN         26
#define DHTTYPE DHT11
DHT dht(DHT11_PIN, DHTTYPE);
int tempValue = 30;
int humiValue = 60;
int tempValueMax = 0;
int humiValueMax = 0;
SimpleKalmanFilter tempfilter(2, 2, 0.001);
SimpleKalmanFilter humifilter(2, 2, 0.001);


//-------------------- Khai báo Button-----------------------------
#include "mybutton.h"
#define BUTTON_DOWN_PIN  34
#define BUTTON_UP_PIN     35
#define BUTTON_SET_PIN    32

#define BUTTON1_ID  1
#define BUTTON2_ID  2
#define BUTTON3_ID  3
Button buttonSET;
Button buttonDOWN;
Button buttonUP;

void button_press_short_callback(uint8_t button_id);
void button_press_long_callback(uint8_t button_id);
// Khai bao LED
#define LED           33
// Khai báo RELAY
#define RELAY         25
// Khai báo BUZZER
#define BUZZER        2
uint32_t timeCountBuzzerWarning = 0;


// Khai báo một vài macro
#define ENABLE  1
#define DISABLE 0
bool autoWarning = DISABLE;
#define TIME_AUTO_WARNING 300  // 300s  =  5 phút

//------------------------- Khai báo wifi -------------------------------
WebServer server(80);//Specify port 
WiFiClient client;
#define AP_MODE 0
#define STA_MODE 1
bool AP_STA_MODE = 1;

#define MODE_WIFI  0
#define MODE_NOWIFI  1
bool modeWIFI = MODE_NOWIFI;
bool tryCbWIFI = MODE_NOWIFI;

//----------------------- Khai báo 1 số biến Blynk -----------------------
bool blynkConnect = true;
BlynkTimer timer; 

//---------------------- Nguyên mẫu hàm  ---------------------------------
void TaskBlynk(void *pvParameters);
void TaskMain(void *pvParameters);
void TaskButton(void *pvParameters);
void TaskDHT11(void *pvParameters);
void TaskSwitchAPtoSTA(void *pvParameters);
void TaskAutoWarning(void *pvParameters);
void TaskOLEDDisplay(void *pvParameters);
void TaskBuzzerWarning(void *pvParameters);
void TaskWindSensor(void *pvParameters);
void TaskRainSensor(void *pvParameters);

void check_weather_and_send_to_blynk(bool autoWarning, int temp, int humi, float rain, float wind);
//-------------------- Khai báo biến freeRTOS ----------------------------
TaskHandle_t TaskButton_handle      = NULL;
TaskHandle_t TaskOLEDDisplay_handle = NULL;
// Định nghĩa 1 số macro
#define TEMP_THRESHOLD_1  25
#define TEMP_THRESHOLD_2  30

#define HUMI_THRESHOLD_1  60
#define HUMI_THRESHOLD_2  70

// --------------------------- Khai báo cảm biến mưa ----------------------------------------
#define rainSensor    15
#define RAIN_ON   0
#define RAIN_OFF  1
int rainCountPulse = 0;                       //  biến đếm xung cb mưa
#define R_Funnel  55                          // đơn vị mm, đây là kích thước bán kính của phễu hứng nước mưa, cần tùy chỉnh theo đúng mô hình thực tế
#define V_AmountOfWater 6                     // đơn vị ml, đây là lượng nước vừa đủ để bập bênh nước lật được, cần căn chỉnh theo thực tế
#define S_Funnel  (3.14*R_Funnel*R_Funnel)    // diện tích mặc đón nước của phễu = pi*r^2
#define onePulseValue  ((1000000/S_Funnel)*V_AmountOfWater)/1000           // với 6ml rót vào ô chứa thì nước sẽ đổ (1 xung)=> tương đương với lượng mưa 0.63135mm, tham khảo cách tính ở đây https://smartsolutions4home.com/ss4h-rg-rain-gauge/
uint32_t timeMillisRain   = 0; 
uint32_t timeCalulateRain = 0; 
#define countSecRain  120                        // thời gian đo, đơn vị giây
float rainfallIntensityValue = 0;                // cường độ mưa, ở đây chúng ta đo lượng mưa tức thời để biết được hiện tại đang mưa to hay mưa nhỏ
float rainfallIntensityValueMax = 0;
                                                 // 1 kiểu đo khác là đo tổng lượng mưa, ví dụ như tổng lượng mưa trong 1 giờ, 12h, 24h
#define RAIN_THRESHOLD_1  2.5
#define RAIN_THRESHOLD_2  7.5
#define RAIN_THRESHOLD_3  50


/*
* Tính toán 1 chút để có được kết quả cường độ mưa
* Ta có mô hình với các thông số như sau:
  Bán kính phễu :55mm
  Lượng nước mưa cần thiết để gàu đo lật : 6ml
* Với mô hình như trên, diện tích của mô hình là S_Funnel = 3.14*55*55= 9498.5 mm2
* 1m2 = 1000000mm2 => tỉ lệ  k= 1000000/9498.5 = 105.28
* Như vậy 1 xung tương đương với A = k*V_AmountOfWater = 105.28*6 = 631.68 ml/m2 = 0.63168 l/m2 
* Đơn vị đo mưa được tính bằng mm có nghĩa là trên 1 đơn vị diện tích có 1 lít nước mưa rơi xuống hoặc trên đơn vị diện tích đó lớp nước mưa có bề dày 1mm. 
* Khi ta nghe bản tin dự báo thời tiết có phần lượng mưa tại một nơi nào đó là 1.0mm thì có nghĩa là ở nơi đó trên 1m2 diện tích có 1ml mưa rơi xuống.
* Như vậy mỗi khi có 6ml nước chảy vào phễu, tương đương với việc ta đọc được 1 xung, cũng tương đương với việc lượng mưa đo được là  0.63168 l/m2 = 0.63168mm
*
* Ví dụ ta tiến hành đo trong 10 phút
* Số xung đo được : 20
* => Lượng nước đo được = 20 * A = 20 * 0.63168 = 12.63 (mm/10 phút) = 12.63*6 = 75.78 mm/h
*/

//-------------------------------- Khai báo cảm biến gió ------------------------------------
#define windSensor        13
#define WIND_ON   0
#define WIND_OFF  1
int windCountPulse = 0;
uint32_t timeMillisWind   = 0; 
uint32_t timeCalulateWind = 0; 
float D_Anemometer = 0.208;             // Đường kính của bộ đo mô hình, đơn vị mét (tùy thuộc vào mô hình, cần chỉnh sửa đúng thực tế)
#define PI  3.14
#define C_Anemometer PI*D_Anemometer    // Chu vi của mô hình, đơn vị mét
#define countSecWind  5                 // thời gian đo, đơn vị giây
float vWindValue = 0;                       // Vận tốc của gió
float vWindValueMax = 0;
#define WIND_THRESHOLD_1  3.3           
#define WIND_THRESHOLD_2  10.7
/*
* Tính toán 1 chút để tính được tốc độ gió, nếu cơ cấu quay của mô hình đủ nhẹ, tốc độ của trục quay sẽ xấp xỉ tốc độ gió
* Ta có mô hình như sau
  Đường kính trục quay : 0.205 m
* Ví dụ với thời gian đo là 1s, ta đo được 5 xung
* 5 xung tương đương với bộ đo đã quay được 5 vòng, vậy quãng đường đã xoay được S = 5 * C_Anemometer = 5 * 0.205 * 3.14 = 3.2185 m
* Với công thức S = v * t => v = S / t = 3.2185/1 = 3.22 m/s
* Như vậy ta đã đo được tốc độ gió tức thời 3.22m/s
* Tuy nhiên ta nên đo thời gian dài hơn để có kết quả chính xác hơn, ví dụ như đo trong 5s, 10s xem được bao nhiêu xung, sau đó chia tỷ lệ để có kết quả chính xác hơn
* Kết quả đo còn phụ thuộc vào phần cứng mô hình, nếu như cánh quay mô hình quá nặng thì kết quả sẽ k được chính xác, cần phải chỉnh theo thực tế
*/

bool rainSensorTrig = 0;
// Hàm ngắt ngoài cảm biến mưa
void IRAM_ATTR rainSensorISR() {
    rainSensorTrig = 1;
}
bool windSensorTrig = 0;
// Hàm ngắt ngoài cảm biến gió
void IRAM_ATTR windSensorISR() {
    windSensorTrig = 1;
}
void setup() {  
  Serial.begin(115200);
  EEPROM.begin(512);

  //---------- Khai báp WiFi ---------
  Serial.println("Configuring access point...");
  WiFi.mode(WIFI_AP_STA);           //Both in Station and Access Point Mode
  // Khởi tạo nút nhấn
  pinMode(BUTTON_SET_PIN, INPUT_PULLUP);
  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);
  button_init(&buttonSET, BUTTON_SET_PIN, BUTTON1_ID);
  button_init(&buttonUP, BUTTON_UP_PIN, BUTTON2_ID);
  button_init(&buttonDOWN,   BUTTON_DOWN_PIN,   BUTTON3_ID);
  button_pressshort_set_callback((void *)button_press_short_callback);
  button_presslong_set_callback((void *)button_press_long_callback);

  // Khởi tạo RELAY
  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, ENABLE);
  // Khởi tạo BUZZER
  pinMode(BUZZER, OUTPUT);
  // Khởi tạo DHT11
  dht.begin();
  // Khởi tạo LED
  pinMode(LED, OUTPUT);
   // Khởi tạo OLED
  oled.begin(i2c_Address, true);
  oled.setTextSize(1);
  oled.setTextColor(SH110X_WHITE);
  oled.clearDisplay();

  oled.setCursor(15, 25);
  oled.print("WEATHER STATION");
  oled.display(); 
  delay(3000);
  // Khởi tạo rainSensor
  pinMode(rainSensor, INPUT_PULLUP);
  // Khởi tạo windSensor
  pinMode(windSensor, INPUT_PULLUP);
  attachInterrupt(rainSensor, rainSensorISR, FALLING);
  attachInterrupt(windSensor, windSensorISR, FALLING);
  // ---------- Kết nối WiFi ---------
  readEEPROM();
  connectSTA();
  // ---------- Đọc giá trị AutoWarning trong EEPROM ----------------
  autoWarning = EEPROM.read(200);
  Serial.print("Auto Warning : ");  Serial.println(autoWarning); 

  // ---------- Khai báo hàm FreeRTOS ---------
  xTaskCreatePinnedToCore(TaskOLEDDisplay,        "TaskOLEDDisplay" ,      1024*8 ,  NULL,  20 ,  &TaskOLEDDisplay_handle  , 1);
  xTaskCreatePinnedToCore(TaskDHT11,              "TaskDHT11" ,            1024*8 ,  NULL,  10  ,  NULL  , 1);
  xTaskCreatePinnedToCore(TaskSwitchAPtoSTA,      "TaskSwitchAPtoSTA" ,    1024*4 ,  NULL,  5  ,  NULL  , 1);
  xTaskCreatePinnedToCore(TaskButton,             "TaskButton" ,           1024*4 ,  NULL,  5  ,  &TaskButton_handle ,  1);
  xTaskCreatePinnedToCore(TaskAutoWarning,        "TaskAutoWarning" ,      1024*4 ,  NULL,  5  ,  NULL ,  1);
  xTaskCreatePinnedToCore(TaskBuzzerWarning,      "TaskBuzzerWarning" ,    1024*4 ,  NULL,  5  ,  NULL ,  1);
  xTaskCreatePinnedToCore(TaskWindSensor,         "TaskWindSensor" ,       1024*4 ,  NULL,  5  ,  NULL ,  1);
  xTaskCreatePinnedToCore(TaskRainSensor,         "TaskRainSensor" ,       1024*4 ,  NULL,  5  ,  NULL ,  1);
  Blynk.virtualWrite(V4, autoWarning);  // autoWarning
  Blynk.virtualWrite(V2, 0);            // rainfallIntensityValue
  Blynk.virtualWrite(V0, 0);            // tempValue
  Blynk.virtualWrite(V1, 0);            // humiValue
}
void loop() { 
  vTaskDelete(NULL);
}

//------------------------------------------------------------------------------
//---------------------------Task Switch AP to STA -----------------------------

//---------------------------If IP is Hitted in Browser ------------------------
void D_AP_SER_Page() {
    int Tnetwork=0,i=0,len=0;
    String st="",s="";
    Tnetwork = WiFi.scanNetworks();//Scan for total networks available
    if(Tnetwork >= 10 ) Tnetwork = 10;
    st = "<ul>";
    for (int i = 0; i < Tnetwork; ++i) {
      // Print SSID and RSSI for each network found
      st += "<li>";
      st +=i + 1;
      st += ": ";
      st += WiFi.SSID(i);
      st += " (";
      st += WiFi.RSSI(i);
      st += ")";
      st += (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*";
      st += "</li>";
    }
    st += "</ul>";
   IPAddress ip = WiFi.softAPIP();             //Get ESP IP Adress
        //String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
        s = "<ul>";
        s = "\n\r\n<!DOCTYPE HTML>\r\n<html><h1>Weather Station</h1> ";
        //s += ipStr;
        s += "<p>";
        s += st;
        s += "<h1> Config Wifi and Token Blynk</h1>";
        s += "<form method='get' action='a'><label style='font-size:30px'>SSID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px'  name='ssid' length=32> <br> <br> <label style='font-size:30px'>Password:</label><input style='font-size: 30px; height:80px' name='pass' length=64> <br><br><label style='font-size:30px'>Tocken:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px' name='token' length=64> <br><br><input  style='font-size:30px;height:60px; width: 100px' type='submit'></form>";
        s += "</ul>";
        s += "</html>\r\n\r\n";
      
    server.send( 200 , "text/html", s);
}
//--------------------------- Get SSID & Password  --------------------------- 
void Get_Req() {
  if (server.hasArg("ssid") && server.hasArg("pass") && server.hasArg("token")) {  
     sssid=server.arg("ssid");//Get SSID
     passs=server.arg("pass");//Get Password
     token=server.arg("token");//Get token
     Serial.println(sssid);
     Serial.println(passs);
     Serial.println(token);  
  }

  if(sssid.length()>1 && passs.length()>1 && token.length()>1) {
      ClearEeprom();//First Clear Eeprom
      delay(10);
      for (int i = 0; i < sssid.length(); ++i)
          EEPROM.write(i, sssid[i]);  
      for (int i = 0; i < passs.length(); ++i)
          EEPROM.write(32+i, passs[i]);
      for (int i = 0; i < token.length(); ++i)
          EEPROM.write(64+i, token[i]);
      
      EEPROM.commit();
          
      String s = "\r\n\r\n<!DOCTYPE HTML>\r\n<html><h1>Weather Station</h1> ";
      s += "<p>Password Saved... Reset to boot into new wifi</html>\r\n\r\n";
      server.send(200,"text/html",s);
    }
    oled.clearDisplay();
    Serial.println(" RESTART ");

    vTaskSuspend(TaskOLEDDisplay_handle);
    oled.setCursor(0,15);
    oled.print(" RESTART ");
    oled.display();
    delay(2000);
    Serial.println("   DONE ");
    oled.setCursor(0,30);
    oled.print(" DONE ");
    oled.display();
    delay(2000);
    
    ESP.restart();
}

//--------------------------- connect STA mode and switch AP Mode if connect fail --------------------------- 
void connectSTA() {
      if ( Essid.length() > 1 ) {  
      Serial.println(Essid);        //Print SSID
      Serial.println(Epass);        //Print Password
      Serial.println(Etoken);        //Print token
      Etoken = Etoken.c_str();
      WiFi.begin(Essid.c_str(), Epass.c_str());   //c_str()
      int countConnect = 0;
      while (WiFi.status() != WL_CONNECTED) {
          delay(500);   
          if(countConnect++  == 20) {
            Serial.println("Ket noi Wifi that bai");
            Serial.println("Kiem tra SSID & PASS");
            Serial.println("Ket noi Wifi: ESP32 de cau hinh");
            Serial.println("IP: 192.168.4.1");

            
            break;
          }
      }
      Serial.println("");
      if(WiFi.status() == WL_CONNECTED) {
        Serial.println("Da ket noi Wifi: ");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP()); 
        Serial.println((char*)Essid.c_str());

        //vTaskSuspend(TaskOLEDDisplay_handle);
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Da ket noi Wifi: ");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print(Essid);
        oled.display(); 
        //vTaskResume(TaskOLEDDisplay_handle);

        strcpy(BLYNK_AUTH_TOKEN,Etoken.c_str());
        
        Blynk.config(BLYNK_AUTH_TOKEN);
        blynkConnect = Blynk.connect();
        if(blynkConnect == false) {
            Serial.println("Chua ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Chua ket noi BLYNK");
            oled.setCursor(0,36);
            oled.print("Kiem tra lai Token");
            oled.display(); delay(3000);

            switchAPMode(); 

            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("Ket noi Wifi: ESP32");
            oled.setCursor(0,24);
            oled.print("de cau hinh lai");
            oled.display(); delay(1000);
            oled.setCursor(0,36);
            oled.print("IP: 192.168.4.1");
            oled.display(); 

            digitalWrite(BUZZER, ENABLE);
            delay(2000);
            digitalWrite(BUZZER, DISABLE);
            delay(3000);
        }
        else {
            Serial.println("Da ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Da ket noi BLYNK");
            oled.display(); delay(2000);
            xTaskCreatePinnedToCore(TaskBlynk,            "TaskBlynk" ,           1024*8 ,  NULL,  5  ,  NULL ,  0);
            timer.setInterval(1000L, myTimer);  
            buzzerBeep(5); 
            return; 
        }
       
        AP_STA_MODE = STA_MODE;
      }
      else {
        switchAPMode(); 
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Ket noi Wifi that bai");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print("Kiem tra SSID & PASS");
        oled.display(); delay(1000);
        oled.setCursor(0,24);
        oled.print("Ket noi Wifi: ESP32");
        oled.setCursor(0,36);
        oled.print("de cau hinh");
        oled.display(); delay(1000);
        oled.setCursor(0,48);
        oled.print("IP: 192.168.4.1");
        oled.display(); 
        digitalWrite(BUZZER, ENABLE);
        delay(2000);
        digitalWrite(BUZZER, DISABLE);
        delay(3000);
      }
        
    }
}

//--------------------------- switch AP Mode --------------------------- 
void switchAPMode() {
  WiFi.softAP(ssid, pass);          //AP SSID and Password(Both in Ap and Sta Mode-According to Library)
  delay(100);                            //Stable AP
  server.on("/",D_AP_SER_Page);
  server.on("/a",Get_Req); 
  Serial.println("In Ap Mode");
  server.begin();  
  delay(300);
}
//--------------------------- Read Eeprom  ------------------------------------
void readEEPROM() {
    for (int i = 0; i < 32; ++i)     //Reading SSID
        Essid += char(EEPROM.read(i)); 
    for (int i = 32; i < 64; ++i)   //Reading Password
        Epass += char(EEPROM.read(i)); 
    for (int i = 64; i < 96; ++i)   //Reading Password
        Etoken += char(EEPROM.read(i)); 
}
//--------------------------- Clear Eeprom  ----------------------------------
void ClearEeprom() {
     Serial.println("Clearing Eeprom");
     for (int i = 0; i < 96; ++i) { EEPROM.write(i, 0); }
}

//---------------------------Task TaskSwitchAPtoSTA---------------------------
void TaskSwitchAPtoSTA(void *pvParameters) {
    while(1) {
        server.handleClient();   
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

//-----------------------------------------------------------------------------
//---------------------------Task Blynk----------------------------------------

//----------------------- Send send Data value to Blynk every 2 seconds--------
bool trigRain = 0;
void myTimer() {
    Blynk.virtualWrite(V0, tempValue);  
    Blynk.virtualWrite(V1, humiValue);
    Blynk.virtualWrite(V3, vWindValue);
    if(trigRain == 1)  {
      Blynk.virtualWrite(V2, rainfallIntensityValue);
      trigRain = 0;
    }
   
}
int virPin = 0;
//------------------------- check autoWarning from BLYNK  -----------------------
BLYNK_WRITE(V4) {
    autoWarning = param.asInt();
    virPin = 4;
    buzzerBeep(1);
    EEPROM.write(200, autoWarning);  EEPROM.commit();
    delay(500);
}

//---------------------------Task Blynk---------------------------
void TaskBlynk(void *pvParameters) {
    while(1) {
      if(blynkConnect == true) {
        Blynk.run();
        timer.run(); 
      }
      vTaskDelay(10/portTICK_PERIOD_MS);
    }
}
//-------------------------- Task OLED Display-------------------------------------------
void TaskOLEDDisplay(void *pvParameters)  {
  delay(15000);
 
  oled.clearDisplay();
  oled.display(); 
  while(1) {
    if(virPin == 4) {
      virPin = 0;
      if(autoWarning == 1) {
        Serial.println("Che do tu dong canh bao:BAT");
        oled.clearDisplay();
        oled.setCursor(0,12);
        oled.print("Che do tu dong ");
        oled.setCursor(0,24);
        oled.print("canh bao:BAT");
        oled.display();
      }
      else {
        Serial.println("Che do tu dong canh bao:TAT");

        oled.clearDisplay();
        oled.setCursor(0,12);
        oled.print("Che do tu dong ");
        oled.setCursor(0,24);
        oled.print("canh bao:TAT");
        oled.display();
      } 
      delay(2000);
    }
    oled.clearDisplay();
    oled.setCursor(0, 0);
    oled.print("Temp:");
    oled.print(tempValue);
    oled.print("*C");

    oled.setCursor(70, 0);
    oled.print("Humi:");
    oled.print(humiValue);
    oled.print("%");

    oled.setCursor(0, 10);
    oled.print("R:");
    oled.print(rainfallIntensityValue);
    oled.print("mm/h");

    oled.setCursor(70, 10);
    oled.print("W:");
    oled.print(vWindValue);
    oled.print("m/s");

    // oled.setCursor(0, 12);
    // oled.print("Soil Moisture : ");
    // oled.print(soilMoistureValue);
    // oled.print("%  ");

    int tempIndex = 0;
    int humiIndex = 0;
    int rainIndex = 0;
    int windIndex = 0;
    if(tempValue < TEMP_THRESHOLD_1 ) tempIndex = 1;
    else if(tempValue >= TEMP_THRESHOLD_1 && tempValue <  TEMP_THRESHOLD_2)  tempIndex = 2;
    else tempIndex = 3;
    oled.setCursor(0, 20);
    oled.print(snOLEDTemp[tempIndex]);
    
    if(humiValue < HUMI_THRESHOLD_1 ) humiIndex = 1;
    else if(humiValue >= HUMI_THRESHOLD_1 && humiValue <= HUMI_THRESHOLD_2)   humiIndex = 2;
    else humiIndex = 3;
    oled.setCursor(0, 30);
    oled.print(snOLEDHumi[humiIndex]);

    if(rainfallIntensityValue == 0 ) rainIndex = 1;
    else if(rainfallIntensityValue > 0 && rainfallIntensityValue <=  RAIN_THRESHOLD_1)  rainIndex = 2;
    else if(rainfallIntensityValue > RAIN_THRESHOLD_1 && rainfallIntensityValue <=  RAIN_THRESHOLD_2)  rainIndex = 3;
    else if(rainfallIntensityValue > RAIN_THRESHOLD_2 && rainfallIntensityValue <=  RAIN_THRESHOLD_3)  rainIndex = 4;
    else rainIndex = 3;
    oled.setCursor(0, 40);
    oled.print(snOLEDRain[rainIndex]);

    if(vWindValue == 0 ) windIndex = 1;
    else if(vWindValue > 0 && vWindValue <=  WIND_THRESHOLD_1)  windIndex = 2;
    else if(vWindValue > WIND_THRESHOLD_1 && vWindValue <=  WIND_THRESHOLD_2)  windIndex = 3;
    else windIndex = 3;
    oled.setCursor(0, 50);
    oled.print(snOLEDWind[windIndex]);
    
    oled.display();
    delay(1000);
  }
}

//-------------------------- Task auto Warning-------------------------------------------
int timeAutoWarning = 0;
void TaskAutoWarning(void *pvParameters)  {
    delay(20000);
    while(1) {
      if(autoWarning == 1 && timeAutoWarning >= TIME_AUTO_WARNING) { 
          // gửi các giá trị cao nhất trong khoảng thời gian đo được lên Blynk
          check_weather_and_send_to_blynk(ENABLE, tempValue, humiValue, rainfallIntensityValueMax, vWindValueMax);
          vWindValueMax = 0;
          rainfallIntensityValueMax = 0;
          timeAutoWarning = 0;
      }
      timeAutoWarning ++;
      delay(1000);
    }
}

//---------------------------Task đo cảm biến mưa---------------------------
void TaskRainSensor(void *pvParameters) {
    while(1) {
      if(rainSensorTrig == 1) {
        delay(150);
        rainSensorTrig = 0;
        rainCountPulse++;
        Serial.print("rainCountPulse : ");
        Serial.println(rainCountPulse);
      }
      if(millis() - timeMillisRain > 1000) {
        timeCalulateRain ++;
        timeMillisRain = millis();
        Serial.print("timeCalulateRain : ");
        Serial.println(timeCalulateRain);
      }
      if(timeCalulateRain >= countSecRain) {
        rainfallIntensityValue = (rainCountPulse*onePulseValue)*(3600/countSecRain);
        rainCountPulse = 0;
        timeCalulateRain = 0;
        Serial.print("rainfallIntensityValue : ");
        Serial.print(rainfallIntensityValue);
        Serial.println("mm/h");
        trigRain = 1;
       
        if(rainfallIntensityValue > rainfallIntensityValueMax) rainfallIntensityValueMax = rainfallIntensityValue;
      }
      vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

//---------------------------Task đo cảm biến gió---------------------------
void TaskWindSensor(void *pvParameters) {
    while(1) {
      if(windSensorTrig == 1) { 
        delay(100);
        windSensorTrig = 0;
        windCountPulse++;
        Serial.print("windCountPulse : ");
        Serial.println(windCountPulse);
      }

      if(millis() - timeMillisWind> 1000) {
        timeCalulateWind ++;
        timeMillisWind = millis();
        //  Serial.print("timeCalulateWind : ");
        //  Serial.println(timeCalulateWind); 
      }
      if(timeCalulateWind >= countSecWind) {
        vWindValue = (windCountPulse * C_Anemometer) / countSecWind;
       
        windCountPulse = 0;
        timeCalulateWind = 0;
        Serial.print("vWindValue : ");
        Serial.print(vWindValue);
        Serial.println("m/s");
       
        if(vWindValue > vWindValueMax) vWindValueMax = vWindValue;
      }
      vTaskDelay(10/portTICK_PERIOD_MS);
    }
}
//-------------------------- Task Buzze Warning------------------------------------------

void TaskBuzzerWarning(void *pvParameters)  {
    delay(20000);
    while(1) {
      int tempIndex = 0;
      int soilMoistureIndex = 0;
      int humiIndex = 0;
      if(autoWarning == 0) {
        if(tempValue < TEMP_THRESHOLD_1 )tempIndex = 1;
        else if(tempValue >= TEMP_THRESHOLD_1 && tempValue <  TEMP_THRESHOLD_2)  tempIndex = 2;
        else tempIndex = 3;
        
        if(humiValue < HUMI_THRESHOLD_1 ) humiIndex = 1;
        else if(humiValue >= HUMI_THRESHOLD_1 && humiValue <= HUMI_THRESHOLD_2)   humiIndex = 2;
        else humiIndex = 3;


        if(tempIndex == 2 || humiIndex == 2 || soilMoistureIndex == 2) blinkLED(1);
        else blinkLED(3);
        delay(3000);
      }
    }
}

//--------------------------------Task TaskDHT11 -------------------------------
void TaskDHT11(void *pvParameters) { 
    delay(10000);
    while(1) {
      int humi =  dht.readHumidity();
      int temp =  dht.readTemperature();
      if (isnan(humi) || isnan(temp) ) {
          Serial.println(F("Failed to read from DHT sensor!"));
      }
      else if(humi <= 100 && temp < 100) {
         // humiValue = humifilter.updateEstimate(humi);
         // tempValue = tempfilter.updateEstimate(temp);
          humiValue = humi;
          tempValue = temp;

          if(humiValue > humiValueMax)  humiValueMax = humiValue;
          if(tempValue > tempValueMax)  tempValueMax = tempValue;
      }
      Serial.print(F("Humidity: "));
      Serial.print(humiValue);
      Serial.print(F("%  Temperature: "));
      Serial.print(tempValue);
      Serial.print(F("°C "));
      Serial.println();
      delay(2000);
    }
}

//-----------------------Task Task Button ----------
void TaskButton(void *pvParameters) {
    while(1) {
      handle_button(&buttonSET);
      handle_button(&buttonUP);
      handle_button(&buttonDOWN);
      delay(10);
    }
}

// ---------------------- Hàm điều khiển còi -----------------------------
void buzzerBeep(int numberBeep) {
  for(int i = 0; i < numberBeep; ++i) {
    digitalWrite(BUZZER, ENABLE);
    delay(100);
    digitalWrite(BUZZER, DISABLE);
    delay(100);
  }  
}
// ---------------------- Hàm điều khiển LED -----------------------------
void blinkLED(int numberBlink) {
  for(int i = 0; i < numberBlink; ++i) {
    digitalWrite(LED, DISABLE);
    delay(300);
    digitalWrite(LED, ENABLE);
    delay(300);
  }  
}
void button_press_short_callback(uint8_t button_id) {
    switch(button_id) {
      case BUTTON1_ID :  
        buzzerBeep(1);
        Serial.println("btSET press short");
        break;
      case BUTTON2_ID :
        buzzerBeep(1);
        Serial.println("btUP press short");
        break;
      case BUTTON3_ID :
        buzzerBeep(1);
        Serial.println("btDOWN press short");
        // check_weather_and_send_to_blynk(ENABLE, tempValue, humiValue, rainfallIntensityValueMax, vWindValueMax);
        // vTaskSuspend(TaskOLEDDisplay_handle);
        // oled.clearDisplay();
        // oled.setCursor(0,12);
        // oled.print("Gui du lieu ");
        // oled.setCursor(0,24);
        // oled.print("den dien thoai");
        // oled.display();
        // delay(2000);
        // vTaskResume(TaskOLEDDisplay_handle);
        break;  
    } 
} 

//-----------------Hàm xử lí nút nhấn giữ ----------------------
void button_press_long_callback(uint8_t button_id) {
    switch(button_id) {  
      case BUTTON1_ID :  
        buzzerBeep(2);
        Serial.println("btSET press long");
        AP_STA_MODE = 1 - AP_STA_MODE;
        switch(AP_STA_MODE) {
          case AP_MODE:
            Serial.println("WiFi AP Mode");
            Serial.println("Connect to WiFi:ESP32");
            Serial.println("192.168.4.1");
            switchAPMode();

            vTaskSuspend(TaskOLEDDisplay_handle);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("Ket noi Wifi: ESP32");
            oled.setCursor(0,24);
            oled.print("de cau hinh");
            oled.display(); delay(1000);
            oled.setCursor(0,36);
            oled.print("IP: 192.168.4.1");
            oled.display(); 
            delay(2000);
            
            break;
          case STA_MODE:
            vTaskSuspend(TaskOLEDDisplay_handle);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("RESTART");
            oled.display(); 
            delay(1000);
            ESP.restart();
            break;
        }
        break;
      case BUTTON2_ID :
        buzzerBeep(2);
        Serial.println("btUP press long");
        vTaskSuspend(TaskOLEDDisplay_handle);
        autoWarning = 1 - autoWarning;
        if(autoWarning == 1) {
          Serial.println("Che do tu dong canh bao:BAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:BAT");
          oled.display();
        }
        else {
          Serial.println("Che do tu dong canh bao:TAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:TAT");
          oled.display();
        } 
        Blynk.virtualWrite(V4, autoWarning); 
        EEPROM.write(200, autoWarning);EEPROM.commit();
        delay(2000);
        vTaskResume(TaskOLEDDisplay_handle);
        break;
      case BUTTON3_ID :
        buzzerBeep(2);
        Serial.println("btDOWN press long");
        break;  
 
   }   
}

//---------------------------------------------------------
/**
 * @brief  gửi lên BLYNK
 *
 * @param autoWarning auto Warning
 * @param temp Nhiệt độ hiện tại    *C
 * @param humi Độ ẩm hiện tại        %
 * @param rain Cường độ mưa          mm/h
 * @param wind Cường độ gió          m/s
 */
void check_weather_and_send_to_blynk(bool autoWarning, int temp, int humi, float rain, float wind) {
  String notifications = "";
  String OLEDnotifications = "";
  if(autoWarning == ENABLE) {
    int tempIndex = 0;
    int humiIndex = 0;
    int rainIndex = 0;
    int windIndex = 0;
    if(tempValue < TEMP_THRESHOLD_1 ) tempIndex = 1;
    else if(tempValue >= TEMP_THRESHOLD_1 && tempValue <  TEMP_THRESHOLD_2)  tempIndex = 2;
    else tempIndex = 3;

    
    if(humiValue < HUMI_THRESHOLD_1 ) humiIndex = 1;
    else if(humiValue >= HUMI_THRESHOLD_1 && humiValue <= HUMI_THRESHOLD_2)   humiIndex = 2;
    else humiIndex = 3;


    if(rainfallIntensityValue == 0 ) rainIndex = 1;
    else if(rainfallIntensityValue > 0 && rainfallIntensityValue <=  RAIN_THRESHOLD_1)  rainIndex = 2;
    else if(rainfallIntensityValue > RAIN_THRESHOLD_1 && rainfallIntensityValue <=  RAIN_THRESHOLD_2)  rainIndex = 3;
    else if(rainfallIntensityValue > RAIN_THRESHOLD_2 && rainfallIntensityValue <=  RAIN_THRESHOLD_3)  rainIndex = 4;
    else rainIndex = 3;


    if(vWindValue == 0 ) windIndex = 1;
    else if(vWindValue > 0 && vWindValue <=  WIND_THRESHOLD_1)  windIndex = 2;
    else if(vWindValue > WIND_THRESHOLD_1 && vWindValue <=  WIND_THRESHOLD_2)  windIndex = 3;
    else windIndex = 3;

    if(windIndex <= 2 &&  rainIndex <= 2 && humiIndex == 2 && tempIndex == 2) {
      Serial.println("Good Weather !!");
    } else {
      Serial.println("Bad Weather !!");
      notifications = snTemp[tempIndex] + String(temp) + "*C . " + snHumi[humiIndex] + String(humi) + "% . " + snWind[windIndex] + String(vWindValue) + "m/s . " + snRain[rainIndex] + String(rainfallIntensityValue) + "mm/h . ";
      Blynk.logEvent("auto_warning",notifications);
    }
  }
 
  Serial.println(notifications);

}
