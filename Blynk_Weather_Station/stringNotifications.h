#ifndef _STRING_NOTIFICATIONS_H_
#define _STRING_NOTIFICATIONS_H_

String snTemp[5] = {"", "Nhiệt độ thấp ","Nhiệt độ phù hợp ","Nhiệt độ an toàn ","Nhiệt độ cao "};
String snHumi[4] = {"", "Độ ẩm không khí thấp ","Độ ẩm không khí phù hợp ","Độ ẩm không khí cao "};
String snWind[5] = {"", "Không có gió ","Gió nhẹ ","Gió vừa ","Gió mạnh "};
String snRain[6] = {"", "Không mưa ","Mưa nhẹ ","Mưa vừa ","Mưa lớn ","Mưa dữ dội "};

String snOLEDTemp[5] = {"", "Nhiet do thap ","Nhiet do phu hop ","Nhiet do an toan ","Nhiet do cao "};
String snOLEDHumi[4] = {"", "Do am kk thap ","Do am kk phu hop ","Do am kk cao "};
String snOLEDWind[5] = {"", "Khong co gio ","Gio nhe ","Gio vua ","Gio manh "};
String snOLEDRain[6] = {"", "Khong mua ","Mua nhe ","Mua vua ","Mua lon ","Mua du doi "};


/*
  0.1 - 3.3  gió nhẹ
  3.4- 10.7  gió vừa
  > 10.7     gió mạnh
*/

/*
  0.1 - 2.5  mưa nhỏ
  2.6- 7.5   mưa vừa
  7.6- 50    mưa lớn
  > 50       mưa dữ dội
*/
#endif