

#define BLYNK_PRINT Serial
// You should get Auth Token in the Blynk App.
#define BLYNK_TEMPLATE_ID           "BANLINHKIEN"
#define BLYNK_TEMPLATE_NAME         "BANLINHKIEN"
char BLYNK_AUTH_TOKEN[32]   =   "";

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <LiquidCrystal.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <EEPROM.h>
#include "config.h"
#include <SimpleKalmanFilter.h>
#include "stringNotifications.h"

// --------------------------- Khai báo cho OLED 1.3 --------------------------------------
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#define i2c_Address 0x3C   //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's
#define SCREEN_WIDTH 128   // OLED display width, in pixels
#define SCREEN_HEIGHT 64   // OLED display height, in pixels
#define OLED_RESET -1      //   QT-PY / XIAO
Adafruit_SH1106G oled = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define OLED_SDA      21
#define OLED_SCL      22
//----------------------- Một số define ---------------------

typedef enum {
  NORMAL,
  CALIB,
  THRESHOLD_SETUP,
  WIFI_SETUP
} MODE;
int modeRun = NORMAL;

// --------------------- Cảm biến DHT11 ---------------------
#include "DHT.h"
#define DHT11_PIN         26
#define DHTTYPE DHT11
DHT dht(DHT11_PIN, DHTTYPE);
int tempValue = 30;
int humiValue = 60;
SimpleKalmanFilter tempfilter(2, 2, 0.001);
SimpleKalmanFilter humifilter(2, 2, 0.001);
// --------------- Khai báo cảm biến khoảng cách srf04 -----------
#include <HCSR04.h>
#define SRF04_TRIG    19
#define SRF04_ECHO    18
HCSR04 ultrasonicSensor(SRF04_TRIG, SRF04_ECHO, 20, 400);
SimpleKalmanFilter srf04filter(2, 2, 0.001);
int srf04Value = 0; // biến đo khoảng cách hiện tại
int srf04ValueCalib = 200; // biến khoảng cách từ cảm biến  đến mặt đất
int waterValue = 0;     // biến chiều cao mực nước,   waterValue = srf04ValueCalib - srf04Value;

int WATER_THRESHOLD = 100;  // nếu waterValue > WATER_THRESHOLD thì sẽ báo động

int tempSrf04ValueCalib = 0; // biến tạm khoảng cách từ cảm biến đến mặt đất
int tempThreshold = 0;       // biến tạm ngưỡng
//-------------------- Khai báo Button----------------------------
#include "mybutton.h"
#define BUTTON_DOWN_PIN  34
#define BUTTON_UP_PIN     35
#define BUTTON_SET_PIN    32

#define BUTTON1_ID  1
#define BUTTON2_ID  2
#define BUTTON3_ID  3
Button buttonSET;
Button buttonDOWN;
Button buttonUP;

void button_press_short_callback(uint8_t button_id);
void button_press_long_callback(uint8_t button_id);
// Khai bao LED
#define LED           33
// Khai báo RELAY
#define RELAY         25
// Khai báo BUZZER
#define BUZZER        2
uint32_t timeCountBuzzerWarning = 0;


// Khai báo một vài macro
#define ENABLE  1
#define DISABLE 0
bool autoWarning = DISABLE;

//------------------------- Khai báo wifi -------------------------------
WebServer server(80);//Specify port 
WiFiClient client;
#define AP_MODE 0
#define STA_MODE 1
bool AP_STA_MODE = 1;

#define MODE_WIFI  0
#define MODE_NOWIFI  1
bool modeWIFI = MODE_NOWIFI;
bool tryCbWIFI = MODE_NOWIFI;

//----------------------- Khai báo 1 số biến Blynk -----------------------
bool blynkConnect = true;
BlynkTimer timer; 

//---------------------- Nguyên mẫu hàm  ---------------------------------
void TaskBlynk(void *pvParameters);
void TaskMain(void *pvParameters);
void TaskButton(void *pvParameters);
void TaskDHT11(void *pvParameters);
void TaskSwitchAPtoSTA(void *pvParameters);
void TaskOLEDDisplay(void *pvParameters);
void TaskBuzzerWarning(void *pvParameters);
void TaskSRF04Sensor(void *pvParameters);


String check_distance_and_send_to_blynk(bool autoWarning, bool sendBlynk, int temp, int humi, int distance);
//-------------------- Khai báo biến freeRTOS ----------------------------
TaskHandle_t TaskButton_handle      = NULL;
TaskHandle_t TaskOLEDDisplay_handle = NULL;
TaskHandle_t TaskBuzzerWarning_handle = NULL;




void setup() {  
  Serial.begin(115200);
  EEPROM.begin(512);

  //---------- Khai báp WiFi ---------
  Serial.println("Configuring access point...");
  WiFi.mode(WIFI_AP_STA);           //Both in Station and Access Point Mode
  // Khởi tạo nút nhấn
  pinMode(BUTTON_SET_PIN, INPUT_PULLUP);
  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);
  button_init(&buttonSET, BUTTON_SET_PIN, BUTTON1_ID);
  button_init(&buttonUP, BUTTON_UP_PIN, BUTTON2_ID);
  button_init(&buttonDOWN,   BUTTON_DOWN_PIN,   BUTTON3_ID);
  button_pressshort_set_callback((void *)button_press_short_callback);
  button_presslong_set_callback((void *)button_press_long_callback);

  // Khởi tạo RELAY
  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, ENABLE);
  // Khởi tạo BUZZER
  pinMode(BUZZER, OUTPUT);
  // Khởi tạo DHT11
  dht.begin();
  // Khởi tạo LED
  pinMode(LED, OUTPUT);
   // Khởi tạo OLED
  oled.begin(i2c_Address, true);
  oled.setTextSize(1);
  oled.setTextColor(SH110X_WHITE);
  oled.clearDisplay();

  oled.setCursor(15, 25);
  oled.print("FLOOD DETECTION");
  oled.setCursor(40, 40);
  oled.print("SYSTEM");
  oled.display(); 
  delay(3000);
  // ---------- Kết nối WiFi ---------
  readEEPROM();
  connectSTA();
  // ---------- Đọc giá trị AutoWarning trong EEPROM ----------------
  autoWarning = EEPROM.read(200);
  Serial.print("Auto Warning : ");  Serial.println(autoWarning); 

  // doc threshold trong eeprom
  
  //----------- Read thresshold from EEPROM --------
  WATER_THRESHOLD = EEPROM.read(202) * 100 + EEPROM.read(203);
  Serial.print("WATER_THRESHOLD : ");
  Serial.println(WATER_THRESHOLD);

  //----------- Read srf04ValueCalib from EEPROM --------
  srf04ValueCalib = EEPROM.read(204) * 100 + EEPROM.read(205);
  Serial.print("srf04ValueCalib : ");
  Serial.println(srf04ValueCalib);
  // Khởi tạo SRF04
  ultrasonicSensor.begin();

  // ---------- Khai báo hàm FreeRTOS ---------
  xTaskCreatePinnedToCore(TaskOLEDDisplay,        "TaskOLEDDisplay" ,      1024*8 ,  NULL,  20 ,  &TaskOLEDDisplay_handle  , 1);
  xTaskCreatePinnedToCore(TaskDHT11,              "TaskDHT11" ,            1024*8 ,  NULL,  5  ,  NULL  , 1);
  xTaskCreatePinnedToCore(TaskSRF04Sensor,         "TaskSRF04Sensor",      1024*4 ,  NULL,  5  ,  NULL  , 1 );
  xTaskCreatePinnedToCore(TaskSwitchAPtoSTA,      "TaskSwitchAPtoSTA" ,    1024*4 ,  NULL,  5  ,  NULL  , 1);
  xTaskCreatePinnedToCore(TaskButton,             "TaskButton" ,           1024*4 ,  NULL,  5  ,  &TaskButton_handle ,  1);
  xTaskCreatePinnedToCore(TaskBuzzerWarning,      "TaskBuzzerWarning" ,    1024*4 ,  NULL,  5  ,  &TaskBuzzerWarning_handle ,  1);
  
  
}
void loop() { 
  vTaskDelete(NULL);
}

//------------------------------------------------------------------------------
//---------------------------Task Switch AP to STA -----------------------------

//---------------------------If IP is Hitted in Browser ------------------------
void D_AP_SER_Page() {
    int Tnetwork=0,i=0,len=0;
    String st="",s="";
    Tnetwork = WiFi.scanNetworks();//Scan for total networks available
    if(Tnetwork >= 10 ) Tnetwork = 10;
    st = "<ul>";
    for (int i = 0; i < Tnetwork; ++i) {
      // Print SSID and RSSI for each network found
      st += "<li>";
      st +=i + 1;
      st += ": ";
      st += WiFi.SSID(i);
      st += " (";
      st += WiFi.RSSI(i);
      st += ")";
      st += (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*";
      st += "</li>";
    }
    st += "</ul>";
   IPAddress ip = WiFi.softAPIP();             //Get ESP IP Adress
        //String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
        s = "<ul>";
        s = "\n\r\n<!DOCTYPE HTML>\r\n<html><h1>Flood Detection System</h1> ";
        //s += ipStr;
        s += "<p>";
        s += st;
        s += "<h1> Config Wifi and Token Blynk</h1>";
        s += "<form method='get' action='a'><label style='font-size:30px'>SSID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px'  name='ssid' length=32> <br> <br> <label style='font-size:30px'>Password:</label><input style='font-size: 30px; height:80px' name='pass' length=64> <br><br><label style='font-size:30px'>Tocken:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px' name='token' length=64> <br><br><input  style='font-size:30px;height:60px; width: 100px' type='submit'></form>";
        s += "</ul>";
        s += "</html>\r\n\r\n";
      
    server.send( 200 , "text/html", s);
}
//--------------------------- Get SSID & Password  --------------------------- 
void Get_Req() {
  if (server.hasArg("ssid") && server.hasArg("pass") && server.hasArg("token")) {  
     sssid=server.arg("ssid");//Get SSID
     passs=server.arg("pass");//Get Password
     token=server.arg("token");//Get token
     Serial.println(sssid);
     Serial.println(passs);
     Serial.println(token);  
  }

  if(sssid.length()>1 && passs.length()>1 && token.length()>1) {
      ClearEeprom();//First Clear Eeprom
      delay(10);
      for (int i = 0; i < sssid.length(); ++i)
          EEPROM.write(i, sssid[i]);  
      for (int i = 0; i < passs.length(); ++i)
          EEPROM.write(32+i, passs[i]);
      for (int i = 0; i < token.length(); ++i)
          EEPROM.write(64+i, token[i]);
      
      EEPROM.commit();
          
      String s = "\r\n\r\n<!DOCTYPE HTML>\r\n<html><h1></h1> ";
      s += "<p>Password Saved... Reset to boot into new wifi</html>\r\n\r\n";
      server.send(200,"text/html",s);
    }
    oled.clearDisplay();
    Serial.println(" RESTART ");

    vTaskSuspend(TaskOLEDDisplay_handle);
    oled.setCursor(0,15);
    oled.print(" RESTART ");
    oled.display();
    delay(2000);
    Serial.println("   DONE ");
    oled.setCursor(0,30);
    oled.print(" DONE ");
    oled.display();
    delay(2000);
    
    ESP.restart();
}

//--------------------------- connect STA mode and switch AP Mode if connect fail --------------------------- 
void connectSTA() {
      if ( Essid.length() > 1 ) {  
      Serial.println(Essid);        //Print SSID
      Serial.println(Epass);        //Print Password
      Serial.println(Etoken);        //Print token
      Etoken = Etoken.c_str();
      WiFi.begin(Essid.c_str(), Epass.c_str());   //c_str()
      int countConnect = 0;
      while (WiFi.status() != WL_CONNECTED) {
          delay(500);   
          if(countConnect++  == 20) {
            Serial.println("Ket noi Wifi that bai");
            Serial.println("Kiem tra SSID & PASS");
            Serial.println("Ket noi Wifi: ESP32IOT de cau hinh");
            Serial.println("IP: 192.168.4.1");
            break;
          }
      }
      Serial.println("");
      if(WiFi.status() == WL_CONNECTED) {
        Serial.println("Da ket noi Wifi: ");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP()); 
        Serial.println((char*)Essid.c_str());

        //vTaskSuspend(TaskOLEDDisplay_handle);
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Da ket noi Wifi: ");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print(Essid);
        oled.display(); 
        //vTaskResume(TaskOLEDDisplay_handle);

        strcpy(BLYNK_AUTH_TOKEN,Etoken.c_str());
        
        Blynk.config(BLYNK_AUTH_TOKEN);
        blynkConnect = Blynk.connect();
        if(blynkConnect == false) {
            Serial.println("Chua ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Chua ket noi BLYNK");
            oled.setCursor(0,36);
            oled.print("Kiem tra lai Token");
            oled.display(); delay(3000);

            switchAPMode(); 

            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("Ket noi Wifi:ESP32IOT");
            oled.setCursor(0,24);
            oled.print("de cau hinh lai");
            oled.display(); delay(1000);
            oled.setCursor(0,36);
            oled.print("IP: 192.168.4.1");
            oled.display(); 

            digitalWrite(BUZZER, ENABLE);
            delay(2000);
            digitalWrite(BUZZER, DISABLE);
            delay(3000);
        }
        else {
            Serial.println("Da ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Da ket noi BLYNK");
            oled.display(); delay(2000);
            xTaskCreatePinnedToCore(TaskBlynk,            "TaskBlynk" ,           1024*8 ,  NULL,  5  ,  NULL ,  0);
            timer.setInterval(1000L, myTimer);  
            buzzerBeep(5); 
            return; 
        }
       
        AP_STA_MODE = STA_MODE;
      }
      else {
        switchAPMode(); 
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Ket noi Wifi that bai");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print("Kiem tra SSID & PASS");
        oled.display(); delay(1000);
        oled.setCursor(0,24);
        oled.print("Ket noi Wifi:ESP32IOT");
        oled.setCursor(0,36);
        oled.print("de cau hinh");
        oled.display(); delay(1000);
        oled.setCursor(0,48);
        oled.print("IP: 192.168.4.1");
        oled.display(); 
        digitalWrite(BUZZER, ENABLE);
        delay(2000);
        digitalWrite(BUZZER, DISABLE);
        delay(3000);
      }
        
    }
}

//--------------------------- switch AP Mode --------------------------- 
void switchAPMode() {
  WiFi.softAP(ssid, pass);          //AP SSID and Password(Both in Ap and Sta Mode-According to Library)
  delay(100);                            //Stable AP
  server.on("/",D_AP_SER_Page);
  server.on("/a",Get_Req); 
  Serial.println("In Ap Mode");
  server.begin();  
  delay(300);
}
//--------------------------- Read Eeprom  ------------------------------------
void readEEPROM() {
    for (int i = 0; i < 32; ++i)     //Reading SSID
        Essid += char(EEPROM.read(i)); 
    for (int i = 32; i < 64; ++i)   //Reading Password
        Epass += char(EEPROM.read(i)); 
    for (int i = 64; i < 96; ++i)   //Reading Password
        Etoken += char(EEPROM.read(i)); 
}
//--------------------------- Clear Eeprom  ----------------------------------
void ClearEeprom() {
     Serial.println("Clearing Eeprom");
     for (int i = 0; i < 96; ++i) { EEPROM.write(i, 0); }
}
// -------- Hàm lưu threshold vào EEPROM , nếu giá trị waterValue > thresshold thì sẽ báo động -----------------------
void writeThresHoldEEPROM(int thresshold)
{
    int firstTwoDigits = thresshold / 100;  // lấy 2 số hàng nghìn và trăm
    int lastTwoDigits  = thresshold % 100;  // lấy 2 số hàng chục và đơn vị
    EEPROM.write(202, firstTwoDigits);         // lưu 2 số hàng nghìn và trăm vào flash
    EEPROM.write(203, lastTwoDigits);          // lưu 2 số hàng chục và đơn vị vào flash
    EEPROM.commit();
    Serial.print("thresshold");
    Serial.println(thresshold);
}
// ---------Hàm lưu Calib vào EEPROM, đây là giá trị từ cảm biến đến mặt đất -----------------------
void writeCalibEEPROM(int calibValue)
{
    int firstTwoDigits = calibValue / 100;  // lấy 2 số hàng nghìn và trăm
    int lastTwoDigits  = calibValue % 100;  // lấy 2 số hàng chục và đơn vị
    EEPROM.write(204, firstTwoDigits);         // lưu 2 số hàng nghìn và trăm vào flash
    EEPROM.write(205, lastTwoDigits);          // lưu 2 số hàng chục và đơn vị vào flash
    EEPROM.commit();
    Serial.print("calibValue");
    Serial.println(calibValue);
}
//---------------------------Task TaskSwitchAPtoSTA---------------------------
void TaskSwitchAPtoSTA(void *pvParameters) {
    while(1) {
        server.handleClient();   
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

//-----------------------------------------------------------------------------
//---------------------------Task Blynk----------------------------------------

//----------------------- Send send Data value to Blynk every 2 seconds--------
void myTimer() {
    Blynk.virtualWrite(V0, tempValue);  
    Blynk.virtualWrite(V1, humiValue);
    Blynk.virtualWrite(V2, waterValue);
    Blynk.virtualWrite(V4, autoWarning); 
    Blynk.virtualWrite(V3, WATER_THRESHOLD); 
}

//--------------Read button from BLYNK and send notification back to Blynk-----------------------

int virPin = 0;
//----------------------- Read thresshold mq2 sensor from Blynk------------------
BLYNK_WRITE(V3) {
    virPin = 3;
    int thresholdBlynk = param.asInt();
    if(thresholdBlynk < srf04ValueCalib)  {
      WATER_THRESHOLD = thresholdBlynk;
      //ghi vao flash
      writeThresHoldEEPROM(WATER_THRESHOLD);
    }
    else {
      delay(500);
      Blynk.virtualWrite(V3, WATER_THRESHOLD); 
    }
 
    delay(500);
}

//------------------------- check autoWarning from BLYNK  -----------------------
BLYNK_WRITE(V4) {
    autoWarning = param.asInt();
    virPin = 4;
    buzzerBeep(1);
    EEPROM.write(200, autoWarning);  EEPROM.commit();
    delay(500);
}

//---------------------------Task TaskSwitchAPtoSTA---------------------------
void TaskBlynk(void *pvParameters) {
    while(1) {
      if(blynkConnect == true) {
        Blynk.run();
        timer.run(); 
      }
      vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

//-------------------------- Task OLED Display-------------------------------------------
int alertOne = 1;
void TaskOLEDDisplay(void *pvParameters)  {
  delay(15000);
  oled.clearDisplay();
  oled.display(); 
  while(1) {
    if(modeRun == NORMAL) {
        if(WATER_THRESHOLD > srf04ValueCalib) {
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("CAU HINH SAI");
            oled.display();
            delay(2000);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("VUI LONG");
            oled.setCursor(0,24);
            oled.print("CAU HINH LAI");
            oled.setCursor(0,36);
            oled.print("THRESHOLD < CALIB");
            oled.display();
            delay(2000);
        }
        else {
            if(virPin == 3) { 
            oled.clearDisplay();
            oled.setCursor(0,30);
            oled.print("Threshold : ");
            oled.setCursor(70,30);
            oled.print(WATER_THRESHOLD);
            oled.display();
            virPin = 0;
            delay(2000);
          }
          else if(virPin == 4) {
            virPin = 0;
            if(autoWarning == 1) {
              Serial.println("Che do tu dong canh bao:BAT");
              oled.clearDisplay();
              oled.setCursor(0,12);
              oled.print("Che do tu dong ");
              oled.setCursor(0,24);
              oled.print("canh bao:BAT");
              oled.display();
            }
            else {
              Serial.println("Che do tu dong canh bao:TAT");
              oled.clearDisplay();
              oled.setCursor(0,12);
              oled.print("Che do tu dong ");
              oled.setCursor(0,24);
              oled.print("canh bao:TAT");
              oled.display();
            } 
            delay(2000);
          }
          oled.clearDisplay();
          oled.setCursor(0, 0);
          oled.print("Water level:");
          oled.print(waterValue);
          oled.print("/");
          oled.print(srf04ValueCalib);
          oled.print("cm");
          
          oled.setCursor(0, 12);
          oled.print("ThresHold: ");
          oled.print(WATER_THRESHOLD);
          oled.print("cm");


          oled.setCursor(0, 24);
          oled.print("Temp:");
          oled.print(tempValue);
          oled.print("*C");

          oled.setCursor(70, 24);
          oled.print("Humi:");
          oled.print(humiValue);
          oled.print("%");

          if(waterValue > WATER_THRESHOLD ) {
            oled.setCursor(10, 36);
            oled.print("                   ");
            oled.setCursor(10, 36);
            oled.print("                   ");
            oled.display();
            delay(300);
            oled.setCursor(23, 40);
            oled.print("MUC NUOC CAO");
            oled.setCursor(33, 54);
            oled.print("NGUY HIEM");
            oled.display();
            if(autoWarning == 1) {
                if(alertOne == 1) {
                  check_distance_and_send_to_blynk(ENABLE, ENABLE, tempValue, humiValue, waterValue); 
                  alertOne = 0;
                }        
            }
          }
          else if(waterValue <= WATER_THRESHOLD )  {
            alertOne = 1;
            oled.setCursor(40, 42);
            oled.print(snOLEDSRF04[1]);
          } 
          oled.display();
        }
        
    }
    else if (modeRun == CALIB) {
        oled.clearDisplay();
        oled.setCursor(0, 0);
        oled.print("KC hien tai : ");
        oled.print(srf04Value);
        oled.print("cm  ");
        
        oled.setCursor(0,20);
        oled.print("NHAN DOWN DE CALIB ");
        oled.setCursor(0,40);
        oled.print("KC cai dat: ");
        oled.print(tempSrf04ValueCalib);
        oled.print("cm");
        oled.display();
    }
    delay(1000);
  }
}

//-------------------------- Task Buzze Warning------------------------------------------
void TaskBuzzerWarning(void *pvParameters)  {
    delay(20000);
    while(1) {
      int srf04Index = 0;
      if(waterValue > WATER_THRESHOLD ) srf04Index = 2;
      else if(waterValue <= WATER_THRESHOLD - 3)   srf04Index = 1;

      if(srf04Index == 1) blinkLED(1);
      else {
        blinkLED(3);
        buzzerBeep(3);
      }
      delay(3000);
    }
}

//--------------------------------Task TaskDHT11 -------------------------------
void TaskDHT11(void *pvParameters) { 
    delay(10000);
    while(1) {
      int humi =  dht.readHumidity();
      int temp =  dht.readTemperature();
      if (isnan(humi) || isnan(temp) ) {
          Serial.println(F("Failed to read from DHT sensor!"));
      }
      else if(humi <= 100 && temp < 100) {
         // humiValue = humifilter.updateEstimate(humi);
         // tempValue = tempfilter.updateEstimate(temp);

          humiValue = humi;
          tempValue = temp;
      }
      Serial.print(F("Humidity: "));
      Serial.print(humiValue);
      Serial.print(F("%  Temperature: "));
      Serial.print(tempValue);
      Serial.print(F("°C "));
      Serial.println();
      delay(2000);
    }
}

//-------------------- Task đọc cảm biến srf04 ---------------

void TaskSRF04Sensor(void *pvParameters) {
    while(1) {
      int  distanceMeasure = ultrasonicSensor.getMedianFilterDistance(); //pass 3 measurements through median filter, better result on moving obstacles
     // distanceMeasure = srf04filter.updateEstimate(distanceMeasure);
     
      if (distanceMeasure < 400) {
        if(distanceMeasure <= 20 ) distanceMeasure = 20;
        Serial.print(distanceMeasure, 1);
        Serial.println(F(" cm"));
        srf04Value = distanceMeasure;
        waterValue = srf04ValueCalib - srf04Value;
        if(waterValue < 0) waterValue = 0;
      }
      else
        Serial.println(F("SRF04 out of range"));
      delay(100);
    }
}

//---------------------- Task Task Button ----------
void TaskButton(void *pvParameters) {
    while(1) {
      handle_button(&buttonSET);
      handle_button(&buttonUP);
      handle_button(&buttonDOWN);
      delay(10);
    }
}


// ---------------------- Hàm điều khiển còi -----------------------------
void buzzerBeep(int numberBeep) {
  for(int i = 0; i < numberBeep; ++i) {
    digitalWrite(BUZZER, ENABLE);
    delay(100);
    digitalWrite(BUZZER, DISABLE);
    delay(100);
  }  
}
// ---------------------- Hàm điều khiển LED -----------------------------
void blinkLED(int numberBlink) {
  for(int i = 0; i < numberBlink; ++i) {
    digitalWrite(LED, DISABLE);
    delay(300);
    digitalWrite(LED, ENABLE);
    delay(300);
  }  
}


void button_press_short_callback(uint8_t button_id) {
    switch(button_id) {
      case BUTTON1_ID :  
        buzzerBeep(1);
        Serial.println("btSET press short");        
        // cai dat threshold
        if(modeRun == NORMAL) {
           modeRun = THRESHOLD_SETUP;
           tempThreshold = WATER_THRESHOLD; 
           vTaskSuspend(TaskOLEDDisplay_handle);
           oled.clearDisplay();
           oled.setCursor(0,0);
           oled.print("THIET LAP NGUONG");
           oled.setCursor(0,15);
           oled.print("NGUONG < ");
           oled.print(srf04ValueCalib);
           oled.setCursor(0,30);
           oled.print(tempThreshold);
           oled.display();
           delay(500);
          
           vTaskResume(TaskOLEDDisplay_handle);
        }
        else if(modeRun == THRESHOLD_SETUP) {
            WATER_THRESHOLD = tempThreshold;
            writeThresHoldEEPROM(WATER_THRESHOLD);
            oled.clearDisplay();
            oled.setCursor(0,15);
            oled.print("THIET LAP NGUONG");
            oled.setCursor(0,30);
            oled.print("THANH CONG");
            oled.display();
            Blynk.virtualWrite(V3, WATER_THRESHOLD);
            modeRun = NORMAL;
            vTaskResume(TaskOLEDDisplay_handle);  
        }

        break;
      case BUTTON2_ID :
        buzzerBeep(1);
        Serial.println("btUP press short");
        if(modeRun == NORMAL) {
            
        }
        else  if(modeRun == THRESHOLD_SETUP) {
            tempThreshold ++;
            if(tempThreshold >= srf04ValueCalib - 10) tempThreshold = srf04ValueCalib -10;
            oled.clearDisplay();
            oled.setCursor(0,0);
            oled.print("THIET LAP NGUONG");
            oled.setCursor(0,15);
            oled.print("NGUONG < ");
            oled.print(srf04ValueCalib);
            oled.setCursor(0,30);
            oled.print(tempThreshold);
            oled.display();
        }
        break;
      case BUTTON3_ID :
        buzzerBeep(1);
        Serial.println("btDOWN press short");
        if(modeRun == NORMAL) {
            // check_distance_and_send_to_blynk(DISABLE, ENABLE, tempValue, humiValue, srf04Value);
            // vTaskSuspend(TaskOLEDDisplay_handle);
            // oled.clearDisplay();
            // oled.setCursor(0,12);
            // oled.print("Gui du lieu ");
            // oled.setCursor(0,24);
            // oled.print("den dien thoai");
            // oled.display();
            // delay(2000);
            // vTaskResume(TaskOLEDDisplay_handle);
        }
        else if (modeRun == CALIB) {
          vTaskSuspend(TaskOLEDDisplay_handle);
          if(srf04Value < 50) {
            Serial.println("lap dat sensor qua thap");
            oled.clearDisplay();
            oled.setCursor(0,15);
            oled.print("LAP DAT SENSOR");
            oled.setCursor(0,30);
            oled.print("QUA THAP < 50cm");
            oled.display();
            delay(2000);
          }
          else if (srf04Value >=50 && srf04Value < 400) {
            tempSrf04ValueCalib = srf04Value;
            Serial.println("LAP DAT SENSOR OK");
            Serial.println(srf04Value);
            oled.clearDisplay();
            oled.setCursor(0,15);
            oled.print("LAP DAT SENSOR");
            oled.setCursor(0,30);
            oled.print("OK : ");
            oled.setCursor(40,30);
            oled.print(srf04Value);
            oled.display();
            delay(2000);
          }
          else {
            Serial.println("SRF04 out of range");
            oled.clearDisplay();
            oled.setCursor(0,15);
            oled.print("KHONG THE ");
            oled.setCursor(0,30);
            oled.print("DO DUOC");
            oled.display();
            delay(2000);
          }
          vTaskResume(TaskOLEDDisplay_handle);  
        }
        else  if(modeRun == THRESHOLD_SETUP) {
            tempThreshold --;
            if(tempThreshold <= 0) tempThreshold = 0;
            oled.clearDisplay();
            oled.setCursor(0,0);
            oled.print("THIET LAP NGUONG");
            oled.setCursor(0,15);
            oled.print("NGUONG < ");
            oled.print(srf04ValueCalib);
            oled.setCursor(0,30);
            oled.print(tempThreshold);
            oled.display();
        }
        
        break;  
    } 
} 

//-----------------Hàm xử lí nút nhấn giữ ----------------------
void button_press_long_callback(uint8_t button_id) {
    switch(button_id) {  
      case BUTTON1_ID :  
        buzzerBeep(2);
        Serial.println("btSET press long");
        AP_STA_MODE = 1 - AP_STA_MODE;
        switch(AP_STA_MODE) {
          case AP_MODE:
            Serial.println("WiFi AP Mode");
            Serial.println("Connect to WiFi : ESP32IOT");
            Serial.println("192.168.4.1");
            switchAPMode();
            vTaskSuspend(TaskOLEDDisplay_handle);
            vTaskSuspend(TaskBuzzerWarning_handle);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("Ket noi Wifi:ESP32IOT");
            oled.setCursor(0,24);
            oled.print("de cau hinh");
            oled.display(); delay(1000);
            oled.setCursor(0,36);
            oled.print("IP: 192.168.4.1");
            oled.display(); 
            delay(2000);
            
            break;
          case STA_MODE:
            vTaskSuspend(TaskOLEDDisplay_handle);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("RESTART");
            oled.display(); 
            delay(1000);
            ESP.restart();
            break;
        }
        break;
      case BUTTON2_ID :
        buzzerBeep(2);
        Serial.println("btUP press long");
        vTaskSuspend(TaskOLEDDisplay_handle);
        autoWarning = 1 - autoWarning;
        if(autoWarning == 1) {
          Serial.println("Che do tu dong canh bao:BAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:BAT");
          oled.display();
        }
        else {
          Serial.println("Che do tu dong canh bao:TAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:TAT");
          oled.display();
        } 
        Blynk.virtualWrite(V4, autoWarning); 
        EEPROM.write(200, autoWarning);EEPROM.commit();
        delay(2000);
        vTaskResume(TaskOLEDDisplay_handle);
        break;
      case BUTTON3_ID :
        buzzerBeep(2);
        if(modeRun == NORMAL) { 
          vTaskSuspend(TaskOLEDDisplay_handle);
          tempSrf04ValueCalib = srf04ValueCalib;
          modeRun = CALIB;
          oled.clearDisplay();
          oled.setCursor(0,15);
          oled.print("THIET LAP");
          oled.setCursor(0,30);
          oled.print("KHOANG CACH");
          oled.display();
          delay(2000);
          oled.clearDisplay();
          oled.setCursor(0,15);
          oled.print("GIU CAM BIEN ");
          oled.setCursor(0,30);
          oled.print("ON DINH");
          oled.display();
          delay(2000);
          vTaskResume(TaskOLEDDisplay_handle);
        }
        else if(modeRun == CALIB) { 
          writeCalibEEPROM(tempSrf04ValueCalib);
          srf04ValueCalib = tempSrf04ValueCalib;
          vTaskSuspend(TaskOLEDDisplay_handle);
          oled.clearDisplay();
          oled.setCursor(0,15);
          oled.print("THIET LAP ");
          oled.setCursor(0,30);
          oled.print("THANH CONG");
          oled.display();
          delay(2000);
          modeRun = NORMAL;
          vTaskResume(TaskOLEDDisplay_handle);
        }
        
        break;  
 
   }   
}

//--------------------------------------------------------
/**
 * @brief Kiểm tra distance và gửi lên BLYNK
 *
 * @param autoWarning auto Warning
 * @param sendBlynk   Có gửi Blynk hay không
 * @param temp Nhiệt độ hiện tại    *C
 * @param humi Độ ẩm hiện tại        %
 * @param water mức nước đo được hiện tại cm
 */
String check_distance_and_send_to_blynk(bool autoWarning, bool sendBlynk, int temp, int humi, int water) {
  String notifications = "";
  String OLEDnotifications = "";
  int srf04Index = 0;

  if(autoWarning == 1) {
      if(waterValue > WATER_THRESHOLD ) srf04Index = 2;
        else if(waterValue <= WATER_THRESHOLD )   srf04Index = 0;

      if(srf04Index == 0)
        notifications = "";
      else {
        if(srf04Index != 0) notifications = notifications + snSRF04[srf04Index] + String(water) + "cm . " ;
        if(sendBlynk == ENABLE)  Blynk.logEvent("auto_warning",notifications);
      }
  }
  Serial.println(notifications);
  return OLEDnotifications;
}
