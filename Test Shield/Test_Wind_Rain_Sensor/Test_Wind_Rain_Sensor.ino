// --------------------------- Khai báo cảm biến mưa ----------------------------------------
#define rainSensor    15
#define RAIN_ON   0
#define RAIN_OFF  1
int rainCountPulse = 0;                    //  biến đếm xung cb mưa
#define R_Funnel  55                       // đơn vị mm, đây là kích thước bán kính của phễu hứng nước mưa, cần tùy chỉnh theo đúng mô hình thực tế
#define V_AmountOfWater 6                  // đơn vị ml, đây là lượng nước vừa đủ để bập bênh nước lật được, cần căn chỉnh theo thực tế
#define S_Funnel  (3.14*R_Funnel*R_Funnel)   // diện tích mặc đón nước của phễu = pi*r^2
#define onePulseValue  ((1000000/S_Funnel)*V_AmountOfWater)/1000           // với 6ml rót vào ô chứa thì nước sẽ đổ (1 xung)=> tương đương với lượng mưa 0.63135mm, tham khảo cách tính ở đây https://smartsolutions4home.com/ss4h-rg-rain-gauge/
uint32_t timeMillisRain   = 0; 
uint32_t timeCalulateRain = 0; 
#define countSecRain  600                   // thời gian đo, đơn vị giây
float rainfallIntensity = 0;                // cường độ mưa, ở đây chúng ta đo lượng mưa tức thời để biết được hiện tại đang mưa to hay mưa nhỏ
                                            // 1 kiểu đo khác là đo tổng lượng mưa, ví dụ như tổng lượng mưa trong 1 giờ, 12h, 24h
/*
* Tính toán 1 chút để có được kết quả cường độ mưa
* Ta có mô hình với các thông số như sau:
  Bán kính phễu :55mm
  Lượng nước mưa cần thiết để gàu đo lật : 6ml
* Với mô hình như trên, diện tích của mô hình là S_Funnel = 3.14*55*55= 9498.5 mm2
* 1m2 = 1000000mm2 => tỉ lệ  k= 1000000/9498.5 = 105.28
* Như vậy 1 xung tương đương với A = k*V_AmountOfWater = 105.28*6 = 631.68 ml/m2 = 0.63168 l/m2 
* Đơn vị đo mưa được tính bằng mm có nghĩa là trên 1 đơn vị diện tích có 1 lít nước mưa rơi xuống hoặc trên đơn vị diện tích đó lớp nước mưa có bề dày 1mm. 
* Khi ta nghe bản tin dự báo thời tiết có phần lượng mưa tại một nơi nào đó là 1.0mm thì có nghĩa là ở nơi đó trên 1m2 diện tích có 1 lít nước mưa rơi xuống.
* Như vậy mỗi khi có 6ml nước chảy vào phễu, tương đương với việc ta đọc được 1 xung, cũng tương đương với việc lượng mưa đo được là 0.63168mm
*
* Ví dụ ta tiến hành đo trong 10 phút
* Số xung đo được : 20
* => Lượng nước đo được = 20 * A = 20 * 0.63168 = 12.63 (mm/10 phút) = 12.63*6 = 75.78 mm/h
*/

//-------------------------------- Khai báo cảm biến gió ------------------------------------
#define windSensor        13
#define WIND_ON   0
#define WIND_OFF  1
int windCountPulse = 0;
uint32_t timeMillisWind   = 0; 
uint32_t timeCalulateWind = 0; 
float D_Anemometer = 0.208;             // Đường kính của bộ đo mô hình, đơn vị mét (tùy thuộc vào mô hình, cần chỉnh sửa đúng thực tế)
#define PI  3.14
#define C_Anemometer PI*D_Anemometer    // Chu vi của mô hình, đơn vị mét
#define countSecWind  5               // thời gian đo, đơn vị giây
float V_Wind = 0;                       // Vận tốc của gió
/*
* Tính toán 1 chút để tính được tốc độ gió, nếu cơ cấu quay của mô hình đủ nhẹ, tốc độ của trục quay sẽ xấp xỉ tốc độ gió
* Ta có mô hình như sau
  Đường kính trục quay : 0.205 m
* Ví dụ với thời gian đo là 1s, ta đo được 5 xung
* 5 xung tương đương với bộ đo đã quay được 5 vòng, vậy quãng đường đã xoay được S = 5 * C_Anemometer = 5 * 0.205 * 3.14 = 3.2185 m
* Với công thức S = v * t => v = S / t = 3.2185/1 = 3.22 m/s
* Như vậy ta đã đo được tốc độ gió tức thời 3.22m/s
* Tuy nhiên ta nên đo thời gian dài hơn để có kết quả chính xác hơn, ví dụ như đo trong 5s, 10s xem được bao nhiêu xung, sau đó chia tỷ lệ để có kết quả chính xác hơn
* Kết quả đo còn phụ thuộc vào phần cứng mô hình, nếu như cánh quay mô hình quá nặng thì kết quả sẽ k được chính xác, cần phải chỉnh theo thực tế
*/
bool rainSensorTrig = 0;
void IRAM_ATTR rainSensorISR() {
    rainSensorTrig = 1;
}
bool windSensorTrig = 0;
void IRAM_ATTR windSensorISR() {
    windSensorTrig = 1;
}
void setup() {
  Serial.begin(9600);
  // Khởi tạo rainSensor
  pinMode(rainSensor, INPUT_PULLUP);
  // Khởi tạo windSensor
  pinMode(windSensor, INPUT_PULLUP);
  attachInterrupt(rainSensor, rainSensorISR, FALLING);
  attachInterrupt(windSensor, windSensorISR, FALLING);
}

void loop() {
  calculateRain();
  calculateWind();
}


void calculateRain() {
  if(rainSensorTrig == 1) {
    delay(150);
    rainSensorTrig = 0;
    rainCountPulse++;
    Serial.print("rainCountPulse : ");
    Serial.println(rainCountPulse);
  }
  if(millis() - timeMillisRain > 1000) {
     timeCalulateRain ++;
     timeMillisRain = millis();
    //  Serial.print("timeMillisRain : ");
    //  Serial.println(timeMillisRain);
  }
  if(timeCalulateRain >= countSecRain) {
    rainfallIntensity = (rainCountPulse*onePulseValue)*(3600/countSecRain);
    rainCountPulse = 0;
    timeCalulateRain = 0;
    Serial.print("rainfallIntensity : ");
    Serial.print(rainfallIntensity);
    Serial.println("mm/h");
  }
}

void calculateWind() {
 if(windSensorTrig == 1) { 
    delay(100);
    windSensorTrig = 0;
    windCountPulse++;
    Serial.print("windCountPulse : ");
    Serial.println(windCountPulse);
  }

  if(millis() - timeMillisWind> 1000) {
     timeCalulateWind ++;
     timeMillisWind = millis();
    //  Serial.print("timeCalulateWind : ");
    //  Serial.println(timeCalulateWind); 
  }
  if(timeCalulateWind >= countSecWind) {
    V_Wind = (windCountPulse * C_Anemometer) / countSecWind;
    windCountPulse = 0;
    timeCalulateWind = 0;
    Serial.print("V_Wind : ");
    Serial.print(V_Wind);
    Serial.println("m/s");
  }
}