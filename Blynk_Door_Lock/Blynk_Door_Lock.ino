#define BLYNK_PRINT Serial
// You should get Auth Token in the Blynk App.
#define BLYNK_TEMPLATE_ID           "BANLINHKIEN"
#define BLYNK_TEMPLATE_NAME         "BANLINHKIEN"
char BLYNK_AUTH_TOKEN[32]   =   "";

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <LiquidCrystal.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <EEPROM.h>
#include "config.h"
#include <SimpleKalmanFilter.h>


// --------------------------- Khai báo cho OLED 1.3 --------------------------------------
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#define i2c_Address 0x3C   //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's
#define SCREEN_WIDTH 128   // OLED display width, in pixels
#define SCREEN_HEIGHT 64   // OLED display height, in pixels
#define OLED_RESET -1      //   QT-PY / XIAO
Adafruit_SH1106G oled = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define OLED_SDA      21
#define OLED_SCL      22


//-------------------- Khai báo Button-----------------------------
#include "mybutton.h"
#define BUTTON_DOWN_PIN   34
#define BUTTON_UP_PIN     35
#define BUTTON_SET_PIN    32

#define BUTTON1_ID  1
#define BUTTON2_ID  2
#define BUTTON3_ID  3
Button buttonSET;
Button buttonDOWN;
Button buttonUP;

void button_press_short_callback(uint8_t button_id);
void button_press_long_callback(uint8_t button_id);
// Khai bao LED
#define LED           33
#define LED_ON        0
#define LED_OFF        1
// Khai báo RELAY
#define RELAY         25
// Khai báo BUZZER
#define BUZZER        2
uint32_t timeCountBuzzerWarning = 0;
// ----------------------------- Khai báo Keypad -------------------------------------------
#include "Adafruit_Keypad.h"
#define KEYPAD_ROW1   4
#define KEYPAD_ROW2   16
#define KEYPAD_ROW3   17
#define KEYPAD_ROW4   5
#define KEYPAD_COL1   12
#define KEYPAD_COL2   14
#define KEYPAD_COL3   27
const byte ROWS = 4; // rows
const byte COLS = 3; // columns
// define the symbols on the buttons of the keypads
char keys[ROWS][COLS] = {
    {'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'}, {'*', '0', '#'}};
byte rowPins[ROWS] = {KEYPAD_ROW1, KEYPAD_ROW2, KEYPAD_ROW3, KEYPAD_ROW4};
byte colPins[COLS] = {KEYPAD_COL1, KEYPAD_COL2, KEYPAD_COL3}; 
Adafruit_Keypad myKeypad = Adafruit_Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS);
keypadEvent e;
typedef enum {
   IDLE_MODE,
   WAIT_CLOSE,
   CHANGE_PASSWORD,
   LOCK_1MIN
}MODE;
uint8_t modeRun = IDLE_MODE;
uint8_t allowAccess = 0;
uint8_t countError = 0;
uint8_t countKey = 0;
char passwordEnter[5] = {0}; 
char password[7] = "1111";            // mật khẩu mặc định
char keyMode;
uint32_t timeMillisAUTO=0;
uint8_t timeClose = 2;                // Thời gian mở khóa
uint8_t notiOne = 0;        

typedef enum {
   ENTER_PASS_1,
   ENTER_PASS_2
}CHANGEPASS;
uint8_t changePassword = ENTER_PASS_1;
char passwordChange1[5] = {0};        // mode đổi pass,mảng lưu pass lần 1
char passwordChange2[5] = {0};        // mode đổi pass,mảng lưu pass lần 2
// -----------------------------------------------------------------------


// Khai báo một vài macro
#define ENABLE  1
#define DISABLE 0
bool autoWarning = DISABLE;


//------------------------- Khai báo wifi --------------------------------
WebServer server(80);//Specify port 
WiFiClient client;
#define AP_MODE 0
#define STA_MODE 1
bool AP_STA_MODE = 1;

#define MODE_WIFI  0
#define MODE_NOWIFI  1
bool modeWIFI = MODE_NOWIFI;
bool tryCbWIFI = MODE_NOWIFI;

//----------------------- Khai báo 1 số biến Blynk -----------------------
bool blynkConnect = true;
BlynkTimer timer; 

//---------------------- Nguyên mẫu hàm  ---------------------------------
void TaskBlynk(void *pvParameters);
void TaskMain(void *pvParameters);
void TaskButton(void *pvParameters);
void TaskSwitchAPtoSTA(void *pvParameters);
void TaskAutoWarning(void *pvParameters);
void TaskOLEDDisplay(void *pvParameters);

//-------------------- Khai báo biến freeRTOS ----------------------------
TaskHandle_t TaskButton_handle      = NULL;
TaskHandle_t TaskOLEDDisplay_handle = NULL;

void setup() {  
  Serial.begin(115200);
  EEPROM.begin(512);

  //---------- Khai báp WiFi ---------
  Serial.println("Configuring access point...");
  WiFi.mode(WIFI_AP_STA);           //Both in Station and Access Point Mode
  // Khởi tạo nút nhấn
  pinMode(BUTTON_SET_PIN, INPUT_PULLUP);
  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);
  button_init(&buttonSET, BUTTON_SET_PIN, BUTTON1_ID);
  button_init(&buttonUP, BUTTON_UP_PIN, BUTTON2_ID);
  button_init(&buttonDOWN,   BUTTON_DOWN_PIN,   BUTTON3_ID);
  button_pressshort_set_callback((void *)button_press_short_callback);
  button_presslong_set_callback((void *)button_press_long_callback);

  // Khởi tạo RELAY
  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, ENABLE);
  // Khởi tạo BUZZER
  pinMode(BUZZER, OUTPUT);
   // Khởi tạo Keypad
  myKeypad.begin();
  // Khởi tạo LED
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LED_OFF);
   // Khởi tạo OLED
  oled.begin(i2c_Address, true);
  oled.setTextSize(1);
  oled.setTextColor(SH110X_WHITE);
  oled.clearDisplay();

  oled.setCursor(15, 25);
  oled.print("DOOR LOCK");
  oled.display(); 
  delay(3000);

  // ---------- Kết nối WiFi ---------
  readEEPROM();
  connectSTA();
  //----------- Đọc pass trong eeprom ----------------------------
  savePASStoBUFF();
  // ---------- Đọc giá trị AutoWarning trong EEPROM ----------------
  autoWarning = EEPROM.read(200);
  Serial.print("Auto Warning : ");  Serial.println(autoWarning); 

  // ---------- Khai báo hàm FreeRTOS ---------
  xTaskCreatePinnedToCore(TaskOLEDDisplay,        "TaskOLEDDisplay" ,      1024*8 ,  NULL,  20 ,  &TaskOLEDDisplay_handle  , 1);
  xTaskCreatePinnedToCore(TaskSwitchAPtoSTA,      "TaskSwitchAPtoSTA" ,    1024*4 ,  NULL,  5  ,  NULL  , 1);
  xTaskCreatePinnedToCore(TaskButton,             "TaskButton" ,           1024*4 ,  NULL,  5  ,  &TaskButton_handle ,  1);
  xTaskCreatePinnedToCore(TaskKeypad,             "TaskKeypad" ,    1024*4 ,  NULL,  5  ,  NULL ,  1);

  Blynk.virtualWrite(V0, 0); 
  Blynk.virtualWrite(V1, autoWarning); 
}
void loop() { 
  vTaskDelete(NULL);
}

//------------------------------------------------------------------------------
//---------------------------Task Switch AP to STA -----------------------------

//---------------------------If IP is Hitted in Browser ------------------------
void D_AP_SER_Page() {
    int Tnetwork=0,i=0,len=0;
    String st="",s="";
    Tnetwork = WiFi.scanNetworks();//Scan for total networks available
    if(Tnetwork >= 10 ) Tnetwork = 10;
    st = "<ul>";
    for (int i = 0; i < Tnetwork; ++i) {
      // Print SSID and RSSI for each network found
      st += "<li>";
      st +=i + 1;
      st += ": ";
      st += WiFi.SSID(i);
      st += " (";
      st += WiFi.RSSI(i);
      st += ")";
      st += (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*";
      st += "</li>";
    }
    st += "</ul>";
   IPAddress ip = WiFi.softAPIP();             //Get ESP IP Adress
        //String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
        s = "<ul>";
        s = "\n\r\n<!DOCTYPE HTML>\r\n<html><h1>DOOR LOCK</h1> ";
        //s += ipStr;
        s += "<p>";
        s += st;
        s += "<h1> Config Wifi and Token Blynk</h1>";
        s += "<form method='get' action='a'><label style='font-size:30px'>SSID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px'  name='ssid' length=32> <br> <br> <label style='font-size:30px'>Password:</label><input style='font-size: 30px; height:80px' name='pass' length=64> <br><br><label style='font-size:30px'>Tocken:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px' name='token' length=64> <br><br><input  style='font-size:30px;height:60px; width: 100px' type='submit'></form>";
        s += "</ul>";
        s += "</html>\r\n\r\n";
      
    server.send( 200 , "text/html", s);
}
//--------------------------- Get SSID & Password  --------------------------- 
void Get_Req() {
  if (server.hasArg("ssid") && server.hasArg("pass") && server.hasArg("token")) {  
     sssid=server.arg("ssid");//Get SSID
     passs=server.arg("pass");//Get Password
     token=server.arg("token");//Get token
     Serial.println(sssid);
     Serial.println(passs);
     Serial.println(token);  
  }

  if(sssid.length()>1 && passs.length()>1 && token.length()>1) {
      ClearEeprom();//First Clear Eeprom
      delay(10);
      for (int i = 0; i < sssid.length(); ++i)
          EEPROM.write(i, sssid[i]);  
      for (int i = 0; i < passs.length(); ++i)
          EEPROM.write(32+i, passs[i]);
      for (int i = 0; i < token.length(); ++i)
          EEPROM.write(64+i, token[i]);
      
      EEPROM.commit();
          
      String s = "\r\n\r\n<!DOCTYPE HTML>\r\n<html><h1>DOOR LOCK</h1> ";
      s += "<p>Password Saved... Reset to boot into new wifi</html>\r\n\r\n";
      server.send(200,"text/html",s);
    }
    oled.clearDisplay();
    Serial.println(" RESTART ");

    vTaskSuspend(TaskOLEDDisplay_handle);
    oled.setCursor(0,15);
    oled.print(" RESTART ");
    oled.display();
    delay(2000);
    Serial.println("   DONE ");
    oled.setCursor(0,30);
    oled.print(" DONE ");
    oled.display();
    delay(2000);
    
    ESP.restart();
}

//--------------------------- connect STA mode and switch AP Mode if connect fail --------------------------- 
void connectSTA() {
      if ( Essid.length() > 1 ) {  
      Serial.println(Essid);        //Print SSID
      Serial.println(Epass);        //Print Password
      Serial.println(Etoken);        //Print token
      Etoken = Etoken.c_str();
      WiFi.begin(Essid.c_str(), Epass.c_str());   //c_str()
      int countConnect = 0;
      while (WiFi.status() != WL_CONNECTED) {
          delay(500);   
          if(countConnect++  == 20) {
            Serial.println("Ket noi Wifi that bai");
            Serial.println("Kiem tra SSID & PASS");
            Serial.println("Ket noi Wifi: ESP32 de cau hinh");
            Serial.println("IP: 192.168.4.1");

            
            break;
          }
      }
      Serial.println("");
      if(WiFi.status() == WL_CONNECTED) {
        Serial.println("Da ket noi Wifi: ");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP()); 
        Serial.println((char*)Essid.c_str());

        //vTaskSuspend(TaskOLEDDisplay_handle);
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Da ket noi Wifi: ");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print(Essid);
        oled.display(); 
        //vTaskResume(TaskOLEDDisplay_handle);

        strcpy(BLYNK_AUTH_TOKEN,Etoken.c_str());
        
        Blynk.config(BLYNK_AUTH_TOKEN);
        blynkConnect = Blynk.connect();
        if(blynkConnect == false) {
            Serial.println("Chua ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Chua ket noi BLYNK");
            oled.setCursor(0,36);
            oled.print("Kiem tra lai Token");
            oled.display(); delay(3000);

            switchAPMode(); 

            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("Ket noi Wifi: ESP32");
            oled.setCursor(0,24);
            oled.print("de cau hinh lai");
            oled.display(); delay(1000);
            oled.setCursor(0,36);
            oled.print("IP: 192.168.4.1");
            oled.display(); 

            digitalWrite(BUZZER, ENABLE);
            delay(2000);
            digitalWrite(BUZZER, DISABLE);
            delay(3000);
        }
        else {
            Serial.println("Da ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Da ket noi BLYNK");
            oled.display(); delay(2000);
            xTaskCreatePinnedToCore(TaskBlynk,            "TaskBlynk" ,           1024*8 ,  NULL,  5  ,  NULL ,  0);
            timer.setInterval(1000L, myTimer);  
            buzzerBeep(5); 
            return; 
        }
       
        AP_STA_MODE = STA_MODE;
      }
      else {
        switchAPMode(); 
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Ket noi Wifi that bai");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print("Kiem tra SSID & PASS");
        oled.display(); delay(1000);
        oled.setCursor(0,24);
        oled.print("Ket noi Wifi: ESP32");
        oled.setCursor(0,36);
        oled.print("de cau hinh");
        oled.display(); delay(1000);
        oled.setCursor(0,48);
        oled.print("IP: 192.168.4.1");
        oled.display(); 
        digitalWrite(BUZZER, ENABLE);
        delay(2000);
        digitalWrite(BUZZER, DISABLE);
        delay(3000);
      }
        
    }
}

//--------------------------- switch AP Mode --------------------------- 
void switchAPMode() {
  WiFi.softAP(ssid, pass);          //AP SSID and Password(Both in Ap and Sta Mode-According to Library)
  delay(100);                            //Stable AP
  server.on("/",D_AP_SER_Page);
  server.on("/a",Get_Req); 
  Serial.println("In Ap Mode");
  server.begin();  
  delay(300);
}
//--------------------------- Read Eeprom  ------------------------------------
void readEEPROM() {
    for (int i = 0; i < 32; ++i)     //Reading SSID
        Essid += char(EEPROM.read(i)); 
    for (int i = 32; i < 64; ++i)   //Reading Password
        Epass += char(EEPROM.read(i)); 
    for (int i = 64; i < 96; ++i)   //Reading Password
        Etoken += char(EEPROM.read(i)); 
}
//--------------------------- Clear Eeprom  ----------------------------------
void ClearEeprom() {
     Serial.println("Clearing Eeprom");
     for (int i = 0; i < 96; ++i) { EEPROM.write(i, 0); }
}

//---------------------------Task TaskSwitchAPtoSTA---------------------------
void TaskSwitchAPtoSTA(void *pvParameters) {
    while(1) {
        server.handleClient();   
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

//-----------------------------------------------------------------------------
//---------------------------Task Blynk----------------------------------------

//----------------------- Send send Data value to Blynk every 2 seconds--------
void myTimer() {
   // Blynk.virtualWrite(V0, tempValue);  
   // Blynk.virtualWrite(V1, humiValue);
}
int virPin = 0;
//------------------------- check autoWarning from BLYNK  -----------------------
BLYNK_WRITE(V1) {
    autoWarning = param.asInt();
    virPin = 1;
    buzzerBeep(1);
    EEPROM.write(200, autoWarning);  EEPROM.commit();
    delay(500);
}

//------------------------- check autoWarning from BLYNK  -----------------------
BLYNK_WRITE(V0) {
    int state = param.asInt();
    if(state == 1) {
        virPin = 2;
        Serial.println("open door");
        controlLock(ENABLE);
        buzzerBeep(3);
    }
    
}
//---------------------------Task Blynk---------------------------
void TaskBlynk(void *pvParameters) {
    while(1) {
      if(blynkConnect == true) {
        Blynk.run();
        timer.run(); 
      }
      vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

//---------------------------Task keypad---------------------------
char starkey[8];
void TaskKeypad(void *pvParameters) {
    while(1) {
      switch(modeRun) {
        case IDLE_MODE:
          allowAccess = 0;
          if(notiOne == 1) {
             digitalWrite(LED, LED_OFF);
             Serial.println("KHoa cua");
             controlLock(DISABLE);
             notiOne = 0;
             oledDisplayPass("NHAP MAT KHAU",""); 
             vTaskResume(TaskOLEDDisplay_handle);
             strcpy(starkey ,"        ");
          }
          myKeypad.tick();
          while(myKeypad.available()){
              e = myKeypad.read();
              if(e.bit.EVENT == KEY_JUST_RELEASED) {
                  vTaskSuspend(TaskOLEDDisplay_handle);
                  buzzerBeep(1);
                  if( (char)e.bit.KEY == '#' ) {
                      countKey=0; 
                      modeRun = IDLE_MODE ;
                      Serial.println("cancel");
                      strcpy(starkey ,"        ");
                      notiOne = 1;
                      
                  } else {
                      passwordEnter[countKey] = (char)e.bit.KEY;
                      Serial.print((char)e.bit.KEY);
                      starkey[countKey*2] = '*'; starkey[countKey*2 + 1] = ' ';
                      countKey++ ;
                      oledDisplayPass("NHAP MAT KHAU",starkey); 
                  }
              }  
            }  
            if(countKey > 3) {
              passwordEnter[4] = '\0';
              delay(50);
              Serial.println(passwordEnter);
              if(strcmp(passwordEnter,password) == 0) {
                  buzzerBeep(3);
                  allowAccess = 1;
                  Serial.println("CORRECT PASSWORD");
                  oledDisplayPass("MAT KHAU DUNG","MOI VAO");
                  strcpy(starkey ,"        ");
                  digitalWrite(LED, LED_ON);
                  timeMillisAUTO = millis();
                  Serial.println("MỞ khóa");
                  controlLock(ENABLE);
                  virPin = 2;
                  modeRun = WAIT_CLOSE; 
              } else {              
                  Serial.println("WRONG PASSWORD");
                  oledDisplayPass("MAT KHAU SAI","");
                  oled.setCursor(20, 40);
                  oled.print("CON ");oled.print(4-countError);oled.print(" LAN THU");
                  oled.display();
                  buzzerWarningBeep(1000);
                  Serial.print(4-countError);
                  Serial.println("lan thu ");
                  delay(1000); 
                  if(autoWarning == 1)
                     Blynk.logEvent("auto_warning","Cảnh báo đột nhập");
                  // if(countError < 4)
                  //   oledDisplayPass("NHAP MAT KHAU","");
                  strcpy(starkey ,"        ");
                  countError ++;   
                  notiOne = 1;
                  if(  countError >= 5)
                      modeRun = LOCK_1MIN;
              }
              countKey = 0;
            }
          break;
        case WAIT_CLOSE:
          // auto lock door
          countError=0; notiOne = 1;
          if(millis() - timeMillisAUTO > timeClose*1000) {
                timeMillisAUTO = millis();
                countKey=0; 
                delay(20);
                modeRun = IDLE_MODE; 
          }
          myKeypad.tick();
          while(myKeypad.available()){
                e = myKeypad.read();
                // đợi k cho về idle mode
                timeMillisAUTO = millis();
                //COI_beep();
                if(e.bit.EVENT == KEY_JUST_RELEASED)  {
                    buzzerBeep(3);
                    keyMode = (char)e.bit.KEY; 
                    Serial.println((char)e.bit.KEY);
                    if(keyMode == '*' ) { 
                      Serial.println("mode doi pass");
                      Serial.println("khoa cua");
                      controlLock(DISABLE);
                      blinkLED(3);
                      oledDisplayPass("NHAP MAT KHAU MOI",""); 
                      strcpy(starkey ,"        "); 
                      modeRun = CHANGE_PASSWORD;

                    } else 
                        modeRun = IDLE_MODE;
                }   
          }      
          break;
        case CHANGE_PASSWORD:
          changePasswordFunc();
          break;
        case LOCK_1MIN:
             notiOne = 1;
            Serial.println("thu lai sau 1 phut");
            buzzerWarningBeep(2000);
            for(int i = 60; i >=0; i --) {
                 oledDisplayPass("THU LAI SAU :","");
                 oled.setCursor(20, 40);
                 oled.print(i);oled.print(" GIAY ");
                 oled.display();
                 Serial.print("TRY AGAIN ");
                 Serial.print(i);
                 Serial.println(" s");
                 delay(1000);
            }
            countError=0;
            modeRun = IDLE_MODE;
            break;   
      }
      delay(10);
    }
}

//---------------------- Hàm đổi mật khẩu ----------------------------------------------


void changePasswordFunc() {
    switch(changePassword) {
          case  ENTER_PASS_1:
               myKeypad.tick();
               while(myKeypad.available()){
                    e = myKeypad.read();
                    // đợi k cho về idle mode
                    timeMillisAUTO = millis();
                    
                     
                    //COI_beep();
                    if(e.bit.EVENT == KEY_JUST_RELEASED) {
                        buzzerBeep(1);
                        if( (char)e.bit.KEY == '#' ) {
                              countKey = 0; 
                              modeRun = IDLE_MODE ;  
                              Serial.println("cancel");
                              notiOne = 1;
                        } else {
                            passwordChange1[countKey] = (char)e.bit.KEY; 
                            Serial.println((char)e.bit.KEY);
                            starkey[countKey*2] = '*'; starkey[countKey*2 + 1] = ' ';
                            countKey++ ;
                            oledDisplayPass("NHAP MAT KHAU MOI",starkey); 
                        }
                    }
               }
               if(countKey > 3) {
                    delay(200);
                    changePassword = ENTER_PASS_2;
                    buzzerBeep(2);
                    countKey  = 0;
                    delay(200);
                    oledDisplayPass("NHAP LAI MAT KHAU",""); 
                    strcpy(starkey ,"        ");
               }
              break;
          case  ENTER_PASS_2:
             myKeypad.tick();
             while(myKeypad.available()){
                  e = myKeypad.read();
                  // đợi k cho về idle mode
                  timeMillisAUTO = millis();
                  //COI_beep();
                  if(e.bit.EVENT == KEY_JUST_RELEASED)  {
                      buzzerBeep(1);
                      if( (char)e.bit.KEY == '#' ) {
                          countKey=0; 
                          modeRun = IDLE_MODE ;
                          Serial.println("cancel");
                          notiOne = 1;
                      } else {
                            passwordChange2[countKey] = (char)e.bit.KEY; 
                            Serial.println((char)e.bit.KEY);
                            starkey[countKey*2] = '*'; starkey[countKey*2 + 1] = ' ';
                            countKey++ ;
                            oledDisplayPass("NHAP LAI MAT KHAU",starkey); 
                      }
                  }
             }
             if(countKey > 3) {
                  delay(50);
                  if(strcmp(passwordChange1, passwordChange2) == 0) {   
                        oledDisplayDoor("DOI MAT KHAU","THANH CONG"); 
                        buzzerBeep(5);
                        blinkLED(3);   
                        passwordChange2[4] = '\0';
                        memcpy (password, passwordChange1, 5);
                        Serial.print("new pass");
                        Serial.println(password);
                        // luu vao flash
                         uint32_t valueCV =convertPassToNumber();
                        savePWtoEEP(valueCV);
                        savePASStoBUFF();
                        Serial.println("CHANGE SUCCESSFUL ");
                        delay(2000);
                        changePassword = ENTER_PASS_1;
                        modeRun = IDLE_MODE ;
                        strcpy(starkey ,"        ");
                        
                  } else {
                        blinkLED(1);
                        oledDisplayDoor("MAT KHAU SAI","NHAP LAI"); 
                        Serial.println("NOT CORRECT");
                        buzzerWarningBeep(1000);
                        changePassword = ENTER_PASS_1;
                        strcpy(starkey ,"        ");
                        oledDisplayPass("NHAP MAT KHAU MOI",""); 
                  }
                  countKey = 0;
             }
             break;
    }     
}

//-------------------------- Task OLED Display-------------------------------------------
void TaskOLEDDisplay(void *pvParameters)  {
  delay(15000);
 
  oled.clearDisplay();
  oled.display(); 
  while(1) {
    if(virPin == 1) {
      virPin = 0;
      if(autoWarning == 1) {
        Serial.println("Che do tu dong canh bao:BAT");
        oled.clearDisplay();
        oled.setCursor(0,12);
        oled.print("Che do tu dong ");
        oled.setCursor(0,24);
        oled.print("canh bao:BAT");
        oled.display();
      }
      else {
        Serial.println("Che do tu dong canh bao:TAT");

        oled.clearDisplay();
        oled.setCursor(0,12);
        oled.print("Che do tu dong ");
        oled.setCursor(0,24);
        oled.print("canh bao:TAT");
        oled.display();
      } 
      delay(2000);
    }
    else if(virPin == 2) {
        virPin = 0;
        oledDisplayDoor("XIN CHAO","");
        delay(2000);
        controlLock(DISABLE);
    }
    oledDisplayDoor("XIN CHAO","");
    delay(1000);
  }
}
void oledDisplayPass(char *s1,char *s2) {
    oled.clearDisplay();
    oled.setCursor(20, 20);
    oled.print(s1);
    oled.setCursor(40, 40);
    oled.print(s2);
    oled.display();
}
void oledDisplayDoor(char *s1,char *s2) {
    oled.clearDisplay();
    oled.setCursor(35, 20);
    oled.print(s1);
    oled.setCursor(35, 40);
    oled.print(s2);
    oled.display();
}

//--------------------------- Task Control Lock --------------------------------
void controlLock(int state) {
    digitalWrite(RELAY, state);
    Blynk.virtualWrite(V0, state);  
}


//-----------------------Task Task Button ----------
void TaskButton(void *pvParameters) {
    while(1) {
      handle_button(&buttonSET);
      handle_button(&buttonUP);
      handle_button(&buttonDOWN);
      delay(10);
    }
}

// ---------------------- Hàm điều khiển còi -----------------------------
void buzzerBeep(int numberBeep) {
  for(int i = 0; i < numberBeep; ++i) {
    digitalWrite(BUZZER, ENABLE);
    delay(100);
    digitalWrite(BUZZER, DISABLE);
    delay(100);
  }  
}
// ---------------------- Hàm điều khiển còi -----------------------------
void buzzerWarningBeep(int time) {

    digitalWrite(BUZZER, ENABLE);
    delay(time);
    digitalWrite(BUZZER, DISABLE);
 
}
// ---------------------- Hàm điều khiển LED -----------------------------
void blinkLED(int numberBlink) {
  for(int i = 0; i < numberBlink; ++i) {
    digitalWrite(LED, LED_ON);
    delay(300);
    digitalWrite(LED, LED_OFF);
    delay(300);
  }  
}
void button_press_short_callback(uint8_t button_id) {
    switch(button_id) {
      case BUTTON1_ID :  
        buzzerBeep(1);
        Serial.println("btSET press short");
        break;
      case BUTTON2_ID :
        buzzerBeep(1);
        Serial.println("btUP press short");
        break;
      case BUTTON3_ID :
        buzzerBeep(1);
        Serial.println("btDOWN press short");
        controlLock(ENABLE);
        buzzerBeep(2);
        virPin = 2;
        break;  
    } 
} 

//-----------------Hàm xử lí nút nhấn giữ ----------------------
void button_press_long_callback(uint8_t button_id) {
    switch(button_id) {  
      case BUTTON1_ID :  
        buzzerBeep(2);
        Serial.println("btSET press long");
        AP_STA_MODE = 1 - AP_STA_MODE;
        switch(AP_STA_MODE) {
          case AP_MODE:
            Serial.println("WiFi AP Mode");
            Serial.println("Connect to WiFi:ESP32");
            Serial.println("192.168.4.1");
            switchAPMode();

            vTaskSuspend(TaskOLEDDisplay_handle);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("Ket noi Wifi: ESP32");
            oled.setCursor(0,24);
            oled.print("de cau hinh");
            oled.display(); delay(1000);
            oled.setCursor(0,36);
            oled.print("IP: 192.168.4.1");
            oled.display(); 
            delay(2000);
            
            break;
          case STA_MODE:
            vTaskSuspend(TaskOLEDDisplay_handle);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("RESTART");
            oled.display(); 
            delay(1000);
            ESP.restart();
            break;
        }
        break;
      case BUTTON2_ID :
        buzzerBeep(2);
        Serial.println("btUP press long");
        vTaskSuspend(TaskOLEDDisplay_handle);
        autoWarning = 1 - autoWarning;
        if(autoWarning == 1) {
          Serial.println("Che do tu dong canh bao:BAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:BAT");
          oled.display();
        }
        else {
          Serial.println("Che do tu dong canh bao:TAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:TAT");
          oled.display();
        } 
        Blynk.virtualWrite(V1, autoWarning); 
        EEPROM.write(200, autoWarning);EEPROM.commit();
        delay(2000);
        vTaskResume(TaskOLEDDisplay_handle);
        break;
      case BUTTON3_ID :
        buzzerBeep(2);
        Serial.println("btDOWN press long");
        break;  
 
   }   
}

// lưu pass từ EEPROM vào mảng
void savePASStoBUFF()
{
    uint32_t c,d;
    c = EEPROM.read(250);
    d = EEPROM.read(251);
    if(c == 255 && d == 255) {
       memcpy (password, "1111", 4);
    } else {
      password[3] =  (char) (d %10 + 48 ) ;
      password[2] =  (char) (d /10 + 48 ) ;
      password[1] =  (char) (c %10 + 48 ) ;
      password[0] =  (char) (c /10 + 48 ) ;  
    }
    Serial.println(password);

}
// chuyển mảng thành số
uint32_t convertPassToNumber()
{
     uint32_t valuee = ((uint32_t)password[3] - 48) + ((uint32_t)password[2]-48)*10 +
    ((uint32_t)password[1]-48)*100 + ((uint32_t)password[0]-48)*1000;
    return valuee;
}

void savePWtoEEP(uint32_t valuePASS)
{
    uint32_t c,d;
    d = valuePASS / 100;
    c = valuePASS % 100;

    EEPROM.write(250, d);EEPROM.commit();
    EEPROM.write(251, c);EEPROM.commit();
    Serial.println( d);
    Serial.println( c);
}
