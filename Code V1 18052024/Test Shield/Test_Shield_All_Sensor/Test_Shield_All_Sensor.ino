#include "DHT.h"
#include "WiFi.h"
// --------------------------- Khai báo cho OLED 1.3 --------------------------------------
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#define i2c_Address 0x3C //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET -1   //   QT-PY / XIAO
Adafruit_SH1106G oled = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define OLED_SDA      21
#define OLED_SCL      22
// -----------------------------------------------------------------------------------------

// ----------------------------- Khai báo Keypad -------------------------------------------
#include "Adafruit_Keypad.h"
#define KEYPAD_ROW1   4
#define KEYPAD_ROW2   16
#define KEYPAD_ROW3   17
#define KEYPAD_ROW4   5
#define KEYPAD_COL1   12
#define KEYPAD_COL2   14
#define KEYPAD_COL3   27
const byte ROWS = 4; // rows
const byte COLS = 3; // columns
// define the symbols on the buttons of the keypads
char keys[ROWS][COLS] = {
    {'1', '2', '3'}, {'4', '5', '6'}, {'7', '8', '9'}, {'*', '0', '#'}};
byte rowPins[ROWS] = {KEYPAD_ROW1, KEYPAD_ROW2, KEYPAD_ROW3, KEYPAD_ROW4};
byte colPins[COLS] = {KEYPAD_COL1, KEYPAD_COL2, KEYPAD_COL3}; 

Adafruit_Keypad myKeypad = Adafruit_Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS);
// ----------------------------------------------------------------------------------------

// ----------------------------- Khai báo SRF04 -------------------------------------------
#include <SimpleKalmanFilter.h>
#include <HCSR04.h>
#define SRF04_TRIG    19
#define SRF04_ECHO    18
HCSR04 ultrasonicSensor(SRF04_TRIG, SRF04_ECHO, 20, 300);
// HCSR04(trigger, echo, temperature, distance)
SimpleKalmanFilter srf04filter(2, 2, 0.001);
// ----------------------------- Khai báo cảm biến bụi ------------------------------------
#include <GP2Y1010AU0F.h>
#define DUST_TRIG             23
#define DUST_ANALOG           36
 GP2Y1010AU0F dustSensor(DUST_TRIG, DUST_ANALOG);


SimpleKalmanFilter dustfilter(2, 2, 0.001);

#define SOIL_MOISTURE 39

//-------------------- Khai báo Button-----------------------------
#include "mybutton.h"
#define BUTTON_DOWN_PIN  34
#define BUTTON_UP_PIN     35
#define BUTTON_SET_PIN    32

#define BUTTON1_ID  1
#define BUTTON2_ID  2
#define BUTTON3_ID  3
Button buttonSET;
Button buttonDOWN;
Button buttonUP;

void button_press_short_callback(uint8_t button_id);
void button_press_long_callback(uint8_t button_id);


#define BUZZER        2
#define LED           33
#define RELAY         25

#define HALL_1        15
#define HALL_2        13


// --------------------- Cảm biến DHT11 ---------------------
#define DHT11_PIN         26
#define DHTTYPE DHT11
DHT dht(DHT11_PIN, DHTTYPE);


void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);

  Serial.println("Setup done");
  // Khởi tạo DHT11
  dht.begin();
  // Khởi tạo LED
  pinMode(LED, OUTPUT);
  // Khởi tạo RELAY
  pinMode(RELAY, OUTPUT);
  // Khởi tạo BUZZER
  pinMode(BUZZER, OUTPUT);
  // Khởi tạo OLED
  oled.begin(i2c_Address, true);
  oled.setTextSize(2);
  oled.setTextColor(SH110X_WHITE);
  // Khởi tạo Keypad
  myKeypad.begin();
  // Khởi tạo HALL1
  pinMode(HALL_1, INPUT_PULLUP);
  // Khởi tạo HALL2
  pinMode(HALL_2, INPUT_PULLUP);
  // Khởi tạo SRF04
  ultrasonicSensor.begin();
  // Khai báo cảm biến bụi
  dustSensor.begin();
  // Khởi tạo nút nhấn
  pinMode(BUTTON_SET_PIN, INPUT_PULLUP);
  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);
  button_init(&buttonSET, BUTTON_SET_PIN, BUTTON1_ID);
  button_init(&buttonUP, BUTTON_UP_PIN, BUTTON2_ID);
  button_init(&buttonDOWN,   BUTTON_DOWN_PIN,   BUTTON3_ID);
  button_pressshort_set_callback((void *)button_press_short_callback);
  button_presslong_set_callback((void *)button_press_long_callback);
  xTaskCreate(sensorTask,   "sensorTask" , 1024*4,    NULL,     5,  NULL); 
  xTaskCreatePinnedToCore(TaskDHT11,           "TaskDHT11" ,          1024*4 ,  NULL,  5 ,  NULL  , 1 );
  xTaskCreatePinnedToCore(TaskDustSensor,      "TaskDustSensor" ,          1024*4 ,  NULL,  5 ,  NULL  , 1 );
  
}

void loop() {
  testKeypad();
  testButton();


}

//--------------------------------Task TaskDHT11 -------------------------------
void TaskDHT11(void *pvParameters) { 
    while(1) {
      measureDHT11();
    }
}
//-----------------------Task Main Display and Control Device----------
void TaskDustSensor(void *pvParameters) {
    while(1) {
      measureDustSensor();
    }
}

void measureDustSensor() {
  int dustValue = dustSensor.read();
  if(dustValue <= 0)  dustValue = 0;
  Serial.print("Dust Density = ");
  Serial.print(dustValue);
  Serial.println(" ug/m3");

  delay(2000);
}

void measureDHT11() {
    int humiValue = dht.readHumidity();
    int tempValue = dht.readTemperature();

    if (isnan(humiValue) || isnan(tempValue) ) {
      Serial.println(F("Failed to read from DHT sensor!"));
    }
    else {

      Serial.print(F("Humidity: "));
      Serial.print(humiValue);
      Serial.print(F("%  Temperature: "));
      Serial.print(tempValue);
      Serial.print(F("°C "));
      Serial.println();
    }
    delay(2000);
   
}


void sensorTask(void * parameter) {
  while(1) {
    testDHT11();
    testLED();
    testRELAY();
    testBUZZER();
    testOLED();
    testHALL1();
    testHALL2();
    testSOILMOISTURE();
    testDistanceSensorSRF04();
    testDustSensor();
    delay(1000);
  }
}

void button_press_short_callback(uint8_t button_id) {
      switch(button_id) {
        case BUTTON1_ID :  
          Serial.println("btSET press short");
          break;
        case BUTTON2_ID :
          Serial.println("btUP press short");
          break;
        case BUTTON3_ID :
          Serial.println("btDOWN press short");
          break;  
   
     } 
} 

//-----------------Hàm xử lí nút nhấn giữ ----------------------
void button_press_long_callback(uint8_t button_id) {
    switch(button_id) {  
      case BUTTON1_ID :  
        Serial.println("btSET press long");
        break;
      case BUTTON2_ID :
        Serial.println("btUP press long");
        break;
      case BUTTON3_ID :
        Serial.println("btDOWN press long");
        break;  
 
   }   
}

void testButton() {
  handle_button(&buttonSET);
  handle_button(&buttonUP);
  handle_button(&buttonDOWN);
}

void testDustSensor() {
  int dustValue = dustSensor.read();
  if(dustValue <= 0)  dustValue = 0;
  Serial.print("Dust Density = ");
  Serial.print(dustValue);
  Serial.println(" ug/m3");
  
  delay(1000);
}
void testDistanceSensorSRF04() {
  int distance = ultrasonicSensor.getMedianFilterDistance(); //pass 3 measurements through median filter, better result on moving obstacles
  //distance = srf04filter.updateEstimate(distance);
    if (distance > 0 && distance < 300) {
      Serial.print(distance, 1);
      Serial.println(F(" cm"));
      oled.clearDisplay();
      oled.setCursor(0, 25);
      oled.print(distance);
      oled.print(" cm");
      oled.display(); 
    }
    else
      Serial.println(F("SRF04 out of range"));

    delay(1000);  
} 


void testSOILMOISTURE() {
  int valueSOILMOISTURE = analogRead(SOIL_MOISTURE);
  Serial.print("SOILMOISTURE value : ");
  Serial.print(valueSOILMOISTURE);
  Serial.println();
  delay(200);
}

void testHALL1() {
  int hallState = digitalRead(HALL_1);
  Serial.print("HALL1 state : ");
  Serial.print(hallState);
  Serial.println();
  delay(200);
}

void testHALL2() {
  int hallState = digitalRead(HALL_2);
  Serial.print("HALL2 state : ");
  Serial.print(hallState);
  Serial.println();
  delay(200);
}

void testKeypad() {
  myKeypad.tick();
  while(myKeypad.available()){
    keypadEvent e = myKeypad.read();
    Serial.print((char)e.bit.KEY);
    if(e.bit.EVENT == KEY_JUST_PRESSED) Serial.println(" pressed");
    else if(e.bit.EVENT == KEY_JUST_RELEASED) Serial.println(" released");
  }
  delay(10);
}

void testOLED() {
   // Clear the buffer.
  oled.clearDisplay();
  uint8_t color = SH110X_WHITE;
  for (int16_t i = 0; i < oled.height() / 2 - 2; i += 2) {
    oled.fillRoundRect(i, i, oled.width() - 2 * i, oled.height() - 2 * i, oled.height() / 4, color);
    if (color == SH110X_WHITE) color = SH110X_BLACK;
    else color = SH110X_WHITE;
    oled.display();
    delay(100);
  }
  delay(1000);
}

void testLED() {
  digitalWrite(LED, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(1000);                      // wait for a second
  digitalWrite(LED, LOW);   // turn the LED off by making the voltage LOW
  delay(1000);  
}

void testRELAY() {
  digitalWrite(RELAY, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(1000);                      // wait for a second
  digitalWrite(RELAY, LOW);   // turn the LED off by making the voltage LOW
  delay(1000);  
}

void testBUZZER() {
  digitalWrite(BUZZER, HIGH);  // turn the LED on (HIGH is the voltage level)
  delay(100);                      // wait for a second
  digitalWrite(BUZZER, LOW);   // turn the LED off by making the voltage LOW
  delay(100);  
}

void testDHT11() {
    delay(2000);
    float h = dht.readHumidity();
    float t = dht.readTemperature();

    if (isnan(h) || isnan(t) ) {
      Serial.println(F("Failed to read from DHT sensor!"));
      return;
    }
    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(F("%  Temperature: "));
    Serial.print(t);
    Serial.print(F("°C "));
    Serial.println();
}


// void testDistanceSensorSRF04()
// {
//   long duration;
//   int distance;
//   digitalWrite(SRF04_TRIG, LOW);
//   delayMicroseconds(2); 
//   digitalWrite(SRF04_TRIG, HIGH); // turn on the Trigger to generate pulse
//   delayMicroseconds(10); // keep the trigger "ON" for 10 ms to generate pulse
//   digitalWrite(SRF04_TRIG, LOW); // Turn off the pulse trigger to stop pulse
//   // If pulse reached the receiver echoPin
//   // become high Then pulseIn() returns the
//   // time taken by the pulse to reach the receiver
//   duration = pulseIn(SRF04_ECHO, HIGH);
//   distance = duration * 0.0344 / 2; 
//   // distance =  kfilter.updateEstimate(distance);
  
//   if(distance<300) {
//     // In ra Serial
//     Serial.print("Distance: ");
//     Serial.print(distance);
//     Serial.println(" cm");

//      // hiển thị ra OLED
//     oled.clearDisplay();
//     oled.setCursor(0, 25);
//     //Display distance in cm
//     oled.print(distance);
//     oled.print(" cm");
//     oled.display(); 
//   }
//   delay(500);
// } 

