
#include <WiFi.h>
#include <WiFiClient.h>
#include <LiquidCrystal.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <EEPROM.h>
#include "config.h"
#include <SimpleKalmanFilter.h>

// --------------------------- Khai báo cho OLED 1.3 --------------------------------------
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#define i2c_Address 0x3C   //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's
#define SCREEN_WIDTH 128   // OLED display width, in pixels
#define SCREEN_HEIGHT 64   // OLED display height, in pixels
#define OLED_RESET -1      //   QT-PY / XIAO
Adafruit_SH1106G oled = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define OLED_SDA      21
#define OLED_SCL      22

//-------------------- Khai báo Button-----------------------------
#include "mybutton.h"
#define BUTTON_DOWN_PIN   34
#define BUTTON_UP_PIN     35
#define BUTTON_SET_PIN    32

#define BUTTON1_ID  1
#define BUTTON2_ID  2
#define BUTTON3_ID  3
Button buttonSET;
Button buttonDOWN;
Button buttonUP;

void button_press_short_callback(uint8_t button_id);
void button_press_long_callback(uint8_t button_id);
// Khai bao LED
#define LED           33
// Khai báo RELAY
#define RELAY         25
// Khai báo BUZZER
#define BUZZER        2
uint32_t timeCountBuzzerWarning = 0;


// Khai báo một vài macro
#define ENABLE  1
#define DISABLE 0
bool autoWarning = DISABLE;

//------------------------- Khai báo wifi -------------------------------
WebServer server(80);//Specify port 
WiFiClient client;
#define AP_MODE 0
#define STA_MODE 1
bool AP_STA_MODE = 1;

#define MODE_WIFI  0
#define MODE_NOWIFI  1
bool modeWIFI = MODE_NOWIFI;
bool tryCbWIFI = MODE_NOWIFI;



TaskHandle_t TaskButton_handle      = NULL;
TaskHandle_t TaskOLEDDisplay_handle = NULL;
TaskHandle_t TaskBuzzerWarning_handle = NULL;

void TaskOLEDDisplay(void *pvParameters) ;
void TaskSwitchAPtoSTA(void *pvParameters) ;
void TaskButton(void *pvParameters) ;
void TaskBuzzerWarning(void *pvParameters) ;

//=========================================================================
// --------------- Khai báo cảm biến khoảng cách srf04 -----------
int WATER_THRESHOLD[3] = {0};        // biến ngưỡng
int distanceValue1 = 20;             // biến giá trị điểm đo 1
int distanceValue2 = 20;             // biến giá trị điểm đo 2
int distanceValue3 = 20;             // biến giá trị điểm đo 3
//=========================================================================
int tempThreshold[3] = {0};       // biến tạm ngưỡng

typedef enum {
  NORMAL,
  THRESHOLD_SETUP,
  WIFI_SETUP
} MODE;
int modeRun = NORMAL;

void setup() {  
  Serial.begin(115200);
  EEPROM.begin(512);

  //---------- Khai báp WiFi ---------
  Serial.println("Configuring access point...");
  WiFi.mode(WIFI_AP_STA);           //Both in Station and Access Point Mode
  // Khởi tạo nút nhấn
  pinMode(BUTTON_SET_PIN, INPUT_PULLUP);
  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);
  button_init(&buttonSET, BUTTON_SET_PIN, BUTTON1_ID);
  button_init(&buttonUP, BUTTON_UP_PIN, BUTTON2_ID);
  button_init(&buttonDOWN,   BUTTON_DOWN_PIN,   BUTTON3_ID);
  button_pressshort_set_callback((void *)button_press_short_callback);
  button_presslong_set_callback((void *)button_press_long_callback);

  // Khởi tạo RELAY
  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, ENABLE);
  // Khởi tạo BUZZER
  pinMode(BUZZER, OUTPUT);

  // Khởi tạo LED
  pinMode(LED, OUTPUT);
   // Khởi tạo OLED
  oled.begin(i2c_Address, true);
  oled.setTextSize(1);
  oled.setTextColor(SH110X_WHITE);
  oled.clearDisplay();

  oled.setCursor(15, 25);
  oled.print("CANH BAO LU LUT");
  oled.display(); 
  delay(3000);
  // ---------- Kết nối WiFi ---------
  readEEPROM();
  connectSTA();
  // Khởi tạo SRF04
  ultrasonicSensor.begin();
  // ---------- Đọc giá trị AutoWarning trong EEPROM ----------------
 // autoWarning = EEPROM.read(200);
 // Serial.print("Auto Warning : ");  Serial.println(autoWarning); 
   //----------- Read thresshold from EEPROM --------
  WATER_THRESHOLD[0] = EEPROM.read(200) * 100 + EEPROM.read(201);
  Serial.print("WATER_THRESHOLD[0] : ");
  Serial.println(WATER_THRESHOLD[0]);

  WATER_THRESHOLD[1] = EEPROM.read(202) * 100 + EEPROM.read(203);
  Serial.print("WATER_THRESHOLD[1] : ");
  Serial.println(WATER_THRESHOLD[1]);

  WATER_THRESHOLD[2] = EEPROM.read(204) * 100 + EEPROM.read(205);
  Serial.print("WATER_THRESHOLD[2] : ");
  Serial.println(WATER_THRESHOLD[2]);


  // ---------- Khai báo hàm FreeRTOS ---------
  xTaskCreatePinnedToCore(TaskOLEDDisplay,        "TaskOLEDDisplay" ,      1024*8 ,  NULL,  20 ,  &TaskOLEDDisplay_handle  , 1);
  xTaskCreatePinnedToCore(TaskSwitchAPtoSTA,      "TaskSwitchAPtoSTA" ,    1024*4 ,  NULL,  5  ,  NULL  , 1);
  xTaskCreatePinnedToCore(TaskButton,             "TaskButton" ,           1024*4 ,  NULL,  5  ,  &TaskButton_handle ,  1);
  xTaskCreatePinnedToCore(TaskBuzzerWarning,      "TaskBuzzerWarning" ,    1024*4 ,  NULL,  5  ,   &TaskBuzzerWarning_handle  ,  1);

}
void loop() { 
  vTaskDelete(NULL);
}

//------------------------------------------------------------------------------
//---------------------------Task Switch AP to STA -----------------------------

//---------------------------If IP is Hitted in Browser ------------------------
void D_AP_SER_Page() {
    int Tnetwork=0,i=0,len=0;
    String st="",s="";
    Tnetwork = WiFi.scanNetworks();//Scan for total networks available
    if(Tnetwork >= 10 ) Tnetwork = 10;
    st = "<ul>";
    for (int i = 0; i < Tnetwork; ++i) {
      // Print SSID and RSSI for each network found
      st += "<li>";
      st +=i + 1;
      st += ": ";
      st += WiFi.SSID(i);
      st += " (";
      st += WiFi.RSSI(i);
      st += ")";
      st += (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*";
      st += "</li>";
    }
    st += "</ul>";
   IPAddress ip = WiFi.softAPIP();             //Get ESP IP Adress
        //String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
        s = "<ul>";
        s = "\n\r\n<!DOCTYPE HTML>\r\n<html><h1>HR THONG CANH BAO LU LUT</h1> ";
        //s += ipStr;
        s += "<p>";
        s += st;
        s += "<h1> Config Wifi and Token Blynk</h1>";
        s += "<form method='get' action='a'><label style='font-size:30px'>SSID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px'  name='ssid' length=32> <br> <br> <label style='font-size:30px'>Password:</label><input style='font-size: 30px; height:80px' name='pass' length=64> <br><br><br><br><input  style='font-size:30px;height:60px; width: 100px' type='submit'></form>";
        s += "</ul>";
        s += "</html>\r\n\r\n";
      
    server.send( 200 , "text/html", s);
}
//--------------------------- Get SSID & Password  --------------------------- 
void Get_Req() {
  if (server.hasArg("ssid") && server.hasArg("pass")) {  
     sssid=server.arg("ssid");//Get SSID
     passs=server.arg("pass");//Get Password
     Serial.println(sssid);
     Serial.println(passs);
  }

  if(sssid.length()>1 && passs.length()>1 ) {
      ClearEeprom();//First Clear Eeprom
      delay(10);
      for (int i = 0; i < sssid.length(); ++i)
          EEPROM.write(i, sssid[i]);  
      for (int i = 0; i < passs.length(); ++i)
          EEPROM.write(32+i, passs[i]);
      
      EEPROM.commit();
          
      String s = "\r\n\r\n<!DOCTYPE HTML>\r\n<html><h1>HR THONG CANH BAO LU LUT</h1> ";
      s += "<p>Password Saved... Reset to boot into new wifi</html>\r\n\r\n";
      server.send(200,"text/html",s);
    }
    oled.clearDisplay();
    Serial.println(" RESTART ");

    vTaskSuspend(TaskOLEDDisplay_handle);
    oled.setCursor(0,15);
    oled.print(" RESTART ");
    oled.display();
    delay(2000);
    Serial.println("   DONE ");
    oled.setCursor(0,30);
    oled.print(" DONE ");
    oled.display();
    delay(2000);
    
    ESP.restart();
}

//--------------------------- connect STA mode and switch AP Mode if connect fail --------------------------- 
void connectSTA() {
      if ( Essid.length() > 1 ) {  
      Serial.println(Essid);        //Print SSID
      Serial.println(Epass);        //Print Password
      WiFi.begin(Essid.c_str(), Epass.c_str());   //c_str()
      int countConnect = 0;
      while (WiFi.status() != WL_CONNECTED) {
          delay(500);   
          if(countConnect++  == 20) {
            Serial.println("Ket noi Wifi that bai");
            Serial.println("Kiem tra SSID & PASS");
            Serial.println("Ket noi Wifi: ESP32 de cau hinh");
            Serial.println("IP: 192.168.4.1");

            
            break;
          }
      }
      Serial.println("");
      if(WiFi.status() == WL_CONNECTED) {
        Serial.println("Da ket noi Wifi: ");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP()); 
        Serial.println((char*)Essid.c_str());

        //vTaskSuspend(TaskOLEDDisplay_handle);
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Da ket noi Wifi: ");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print(Essid);
        oled.display(); 
        //vTaskResume(TaskOLEDDisplay_handle);
        AP_STA_MODE = STA_MODE;
      }
      else {
        switchAPMode(); 
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Ket noi Wifi that bai");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print("Kiem tra SSID & PASS");
        oled.display(); delay(1000);
        oled.setCursor(0,24);
        oled.print("Ket noi Wifi: ESP32");
        oled.setCursor(0,36);
        oled.print("de cau hinh");
        oled.display(); delay(1000);
        oled.setCursor(0,48);
        oled.print("IP: 192.168.4.1");
        oled.display(); 
        digitalWrite(BUZZER, ENABLE);
        delay(2000);
        digitalWrite(BUZZER, DISABLE);
        delay(3000);
      }
        
    }
}

//--------------------------- switch AP Mode --------------------------- 
void switchAPMode() {
  WiFi.softAP(ssid, pass);          //AP SSID and Password(Both in Ap and Sta Mode-According to Library)
  delay(100);                            //Stable AP
  server.on("/",D_AP_SER_Page);
  server.on("/a",Get_Req); 
  Serial.println("In Ap Mode");
  server.begin();  
  delay(300);
}
//--------------------------- Read Eeprom  ------------------------------------
void readEEPROM() {
    for (int i = 0; i < 32; ++i)     //Reading SSID
        Essid += char(EEPROM.read(i)); 
    for (int i = 32; i < 64; ++i)   //Reading Password
        Epass += char(EEPROM.read(i)); 
}
//--------------------------- Clear Eeprom  ----------------------------------
void ClearEeprom() {
     Serial.println("Clearing Eeprom");
     for (int i = 0; i < 64; ++i) { EEPROM.write(i, 0); }
}

//---------------------------Task TaskSwitchAPtoSTA---------------------------
void TaskSwitchAPtoSTA(void *pvParameters) {
    while(1) {
        server.handleClient();   
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

bool warningTrig = DISABLE;

//-------------------------- Task OLED Display-------------------------------------------
void TaskOLEDDisplay(void *pvParameters)  {
  delay(15000);
 
  oled.clearDisplay();
  oled.display(); 
  while(1) {

    oled.clearDisplay();
    oled.setCursor(0, 0);
    oled.print("DD1:");
    oled.print(distanceValue1);
    oled.print("cm");
    oled.setCursor(70, 0);
    oled.print("N: ");
    oled.print(WATER_THRESHOLD[0]);
    oled.print("cm");

    oled.setCursor(0, 12);
    oled.print("DD2:");
    oled.print(distanceValue2);
    oled.print("cm");
    oled.setCursor(70, 12);
    oled.print("N: ");
    oled.print(WATER_THRESHOLD[1]);
    oled.print("cm");

    oled.setCursor(0, 24);
    oled.print("DD3:");
    oled.print(distanceValue3);
    oled.print("cm");
    oled.setCursor(70, 24);
    oled.print("N: ");
    oled.print(WATER_THRESHOLD[2]);
    oled.print("cm");

    String str= "";
    if(distanceValue1 > WATER_THRESHOLD[0]) {
       str +="1 "; warningTrig = ENABLE;
    }

    if(distanceValue2 > WATER_THRESHOLD[1]) {
       str +="2 "; warningTrig = ENABLE;
    }
    if(distanceValue2 > WATER_THRESHOLD[2]) {
       str +="3 "; warningTrig = ENABLE;
    }
    
    if(distanceValue1 < WATER_THRESHOLD[0] && distanceValue2 < WATER_THRESHOLD[1] && distanceValue3 < WATER_THRESHOLD[2] ) {
       warningTrig = DISABLE;
    }

    if(warningTrig == ENABLE) {
      oled.setCursor(0, 36);
      oled.print("                   ");
      oled.setCursor(0, 48);
      oled.print("                   ");
      oled.display();
      delay(300);
      oled.setCursor(0, 36);
      oled.print("CANH BAO NGUY HIEM");
      oled.setCursor(0, 48);
      oled.print("DIEM DO: ");
      oled.print(str);
    } else {
      oled.setCursor(40, 44);
      oled.print("AN TOAN");
    }
    
    
    oled.display();
    delay(1000);
  }
}

// -------- Hàm lưu threshold vào EEPROM , nếu giá trị waterValue > thresshold thì sẽ báo động , điểm đo truyền 0, 1, 2 , 3,  -----------------------
void writeThresHoldEEPROM(int diemdo,int thresshold) {
    int firstTwoDigits = thresshold / 100;  // lấy 2 số hàng nghìn và trăm
    int lastTwoDigits  = thresshold % 100;  // lấy 2 số hàng chục và đơn vị

    EEPROM.write(200 + 2*diemdo, firstTwoDigits);         // lưu 2 số hàng nghìn và trăm vào flash
    EEPROM.write(200 + 2*diemdo+1, lastTwoDigits);          // lưu 2 số hàng chục và đơn vị vào flash
    EEPROM.commit();
    Serial.print("thresshold");
    Serial.println(thresshold);
}


//-------------------- Task đọc cảm biến srf04 ---------------
void TaskSRF04Sensor(void *pvParameters) {
    while(1) {
      int  distanceMeasure = ultrasonicSensor.getMedianFilterDistance(); //pass 3 measurements through median filter, better result on moving obstacles
     
      if (distanceMeasure < 400) {
        if(distanceMeasure <= 20 ) distanceMeasure = 20;
        Serial.print(distanceMeasure, 1);
        Serial.println(F(" cm"));
        distanceValue1 = distanceMeasure;
      }
      else
        Serial.println(F("SRF04 out of range"));
      delay(1000);
    }
}
//-------------------------- Task Buzze Warning------------------------------------------

void TaskBuzzerWarning(void *pvParameters)  {
    delay(20000);
    while(1) {
        if(warningTrig == ENABLE) {
           buzzerBeep2(4);
        }
        delay(10);
    
    }
}



//-----------------------Task Task Button ----------
void TaskButton(void *pvParameters) {
    while(1) {
      handle_button(&buttonSET);
      handle_button(&buttonUP);
      handle_button(&buttonDOWN);
      delay(10);
    }
}


// ---------------------- Hàm điều khiển còi -----------------------------
void buzzerBeep(int numberBeep) {
  for(int i = 0; i < numberBeep; ++i) {
    digitalWrite(BUZZER, ENABLE);
    delay(100);
    digitalWrite(BUZZER, DISABLE);
    delay(100);
  }  
}
// ---------------------- Hàm điều khiển còi -----------------------------
void buzzerBeep2(int numberBeep) {
  for(int i = 0; i < numberBeep; ++i) {
    digitalWrite(BUZZER, ENABLE);
    delay(300);
    digitalWrite(BUZZER, DISABLE);
    delay(300);
  }  
}
// ---------------------- Hàm điều khiển LED -----------------------------
void blinkLED(int numberBlink) {
  for(int i = 0; i < numberBlink; ++i) {
    digitalWrite(LED, DISABLE);
    delay(300);
    digitalWrite(LED, ENABLE);
    delay(300);
  }  
}

int pickPoint = 0;
void button_press_short_callback(uint8_t button_id) {
    switch(button_id) {
      case BUTTON1_ID :  
        buzzerBeep(1);
        Serial.println("btSET press short");
        if(modeRun == THRESHOLD_SETUP) {
          pickPoint ++;
          if(pickPoint > 2) pickPoint = 0;
          Serial.print("pickPoint :");Serial.println(pickPoint);
          oled.clearDisplay();    
          oled.setCursor(0,15);
          oled.print("THIET LAP NGUONG");
          oled.setCursor(0,30);
          oled.print("DIEM DO "); oled.print(pickPoint + 1);
          oled.setCursor(0,45);
          oled.print(tempThreshold[pickPoint]);
          oled.display();
        }
        break;
      case BUTTON2_ID :
        buzzerBeep(1);
        Serial.println("btUP press short");
        if(modeRun == THRESHOLD_SETUP) {
            tempThreshold[pickPoint] ++;
            if(tempThreshold[pickPoint] >= 100) tempThreshold[pickPoint] =100;
            oled.clearDisplay();          
            oled.setCursor(0,15);
            oled.print("THIET LAP NGUONG");
            oled.setCursor(0,30);
            oled.print("DIEM DO "); oled.print(pickPoint + 1);
            oled.setCursor(0,45);
            oled.print(tempThreshold[pickPoint]);
            oled.display();
        }
        break;
      case BUTTON3_ID :
        buzzerBeep(1);
        Serial.println("btDOWN press short");
        if(modeRun == THRESHOLD_SETUP) {
            tempThreshold[pickPoint] --;
            if(tempThreshold[pickPoint] <= 0) tempThreshold[pickPoint] = 0;
            oled.clearDisplay();
            oled.setCursor(0,15);
            oled.print("THIET LAP NGUONG");
            oled.setCursor(0,30);
            oled.print("DIEM DO "); oled.print(pickPoint + 1);
            oled.setCursor(0,45);
            oled.print(tempThreshold[pickPoint]);

            oled.display();
        }
        break;  
    } 
} 

//-----------------Hàm xử lí nút nhấn giữ ----------------------
void button_press_long_callback(uint8_t button_id) {
    switch(button_id) {  
      case BUTTON1_ID : 
        digitalWrite(BUZZER, DISABLE); 
        buzzerBeep(2);
        Serial.println("btSET press long");
        if(modeRun == NORMAL) {
            AP_STA_MODE = 1 - AP_STA_MODE;
            switch(AP_STA_MODE) {
              case AP_MODE:
                Serial.println("WiFi AP Mode");
                Serial.println("Connect to WiFi:ESP32");
                Serial.println("192.168.4.1");
                switchAPMode();

                vTaskSuspend(TaskOLEDDisplay_handle);
                oled.clearDisplay();
                oled.setCursor(0,12);
                oled.print("Ket noi Wifi: ESP32");
                oled.setCursor(0,24);
                oled.print("de cau hinh");
                oled.display(); delay(1000);
                oled.setCursor(0,36);
                oled.print("IP: 192.168.4.1");
                oled.display(); 
                delay(2000);
                
                break;
              case STA_MODE:
                vTaskSuspend(TaskOLEDDisplay_handle);
                oled.clearDisplay();
                oled.setCursor(0,12);
                oled.print("RESTART");
                oled.display(); 
                delay(1000);
                ESP.restart();
                break;
            }
        }
       
        break;
      case BUTTON2_ID :
        buzzerBeep(2);
        Serial.println("btUP press long");
        
        break;
      case BUTTON3_ID :
        digitalWrite(BUZZER, DISABLE);
        buzzerBeep(2);
        Serial.println("btDOWN press long");
         if(modeRun == NORMAL) {
           modeRun = THRESHOLD_SETUP;
           tempThreshold[0] = WATER_THRESHOLD[0]; 
           tempThreshold[1] = WATER_THRESHOLD[1]; 
           tempThreshold[2] = WATER_THRESHOLD[2]; 
           vTaskSuspend(TaskOLEDDisplay_handle);
           vTaskSuspend(TaskBuzzerWarning_handle);       
           oled.clearDisplay();
           oled.setCursor(0,15);
           oled.print("THIET LAP NGUONG");
           oled.setCursor(0,30);
           oled.print("DIEM DO "); oled.print(pickPoint + 1);
           oled.setCursor(0,45);
           oled.print(tempThreshold[pickPoint]);
           oled.display();
           delay(500);
        }
        else if(modeRun == THRESHOLD_SETUP) {
            WATER_THRESHOLD[0] = tempThreshold[0];
            WATER_THRESHOLD[1] = tempThreshold[1];
            WATER_THRESHOLD[2] = tempThreshold[2];
            writeThresHoldEEPROM(0, WATER_THRESHOLD[0]);
            writeThresHoldEEPROM(1, WATER_THRESHOLD[1]);
            writeThresHoldEEPROM(2, WATER_THRESHOLD[2]);
            oled.clearDisplay();
            oled.setCursor(0,15);
            oled.print("THIET LAP NGUONG");
            oled.setCursor(0,30);
            oled.print("THANH CONG");
            oled.display();
            delay(1000);
            modeRun = NORMAL;
            pickPoint = 0;
            vTaskResume(TaskOLEDDisplay_handle); 
            vTaskResume(TaskBuzzerWarning_handle); 
        }
        break;  
 
   }   
}


