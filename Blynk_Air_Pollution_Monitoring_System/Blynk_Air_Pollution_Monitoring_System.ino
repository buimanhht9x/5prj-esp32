#define BLYNK_PRINT Serial
// You should get Auth Token in the Blynk App.
#define BLYNK_TEMPLATE_ID           "BANLINHKIEN"
#define BLYNK_TEMPLATE_NAME         "BANLINHKIEN"
char BLYNK_AUTH_TOKEN[32]   =   "";

#include <WiFi.h>
#include <WiFiClient.h>
#include <BlynkSimpleEsp32.h>
#include <LiquidCrystal.h>
#include <WebServer.h>
#include <ESPmDNS.h>
#include <EEPROM.h>
#include "config.h"
#include <SimpleKalmanFilter.h>
#include "stringNotifications.h"

// --------------------------- Khai báo cho OLED 1.3 --------------------------------------
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH110X.h>

#define i2c_Address 0x3C   //initialize with the I2C addr 0x3C Typically eBay OLED's
//#define i2c_Address 0x3d //initialize with the I2C addr 0x3D Typically Adafruit OLED's
#define SCREEN_WIDTH 128   // OLED display width, in pixels
#define SCREEN_HEIGHT 64   // OLED display height, in pixels
#define OLED_RESET -1      //   QT-PY / XIAO
Adafruit_SH1106G oled = Adafruit_SH1106G(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES 10
#define XPOS 0
#define YPOS 1
#define DELTAY 2

#define OLED_SDA      21
#define OLED_SCL      22
// --------------------- Cảm biến DHT11 ---------------------
#include "DHT.h"
#define DHT11_PIN         26
#define DHTTYPE DHT11
DHT dht(DHT11_PIN, DHTTYPE);
int tempValue = 30;
int humiValue = 60;
SimpleKalmanFilter tempfilter(2, 2, 0.001);
SimpleKalmanFilter humifilter(2, 2, 0.001);
// ----------------------------- Khai báo cảm biến bụi ------------------------------------
#include <GP2Y1010AU0F.h>
#define DUST_TRIG             23
#define DUST_ANALOG           36
GP2Y1010AU0F dustSensor(DUST_TRIG, DUST_ANALOG);
SimpleKalmanFilter dustfilter(2, 2, 0.001);
int dustValue = 10;
//-------------------- Khai báo Button-----------------------------
#include "mybutton.h"
#define BUTTON_DOWN_PIN  34
#define BUTTON_UP_PIN     35
#define BUTTON_SET_PIN    32

#define BUTTON1_ID  1
#define BUTTON2_ID  2
#define BUTTON3_ID  3
Button buttonSET;
Button buttonDOWN;
Button buttonUP;

void button_press_short_callback(uint8_t button_id);
void button_press_long_callback(uint8_t button_id);
// Khai bao LED
#define LED           33
// Khai báo RELAY
#define RELAY         25
// Khai báo BUZZER
#define BUZZER        2
uint32_t timeCountBuzzerWarning = 0;
#define TIME_BUZZER_WARNING     300  //thời gian cảnh báo bằng còi (đơn vị giây)

// Khai báo một vài macro
#define ENABLE  1
#define DISABLE 0
bool autoWarning = DISABLE;

//------------------------- Khai báo wifi -------------------------------
WebServer server(80);//Specify port 
WiFiClient client;
#define AP_MODE 0
#define STA_MODE 1
bool AP_STA_MODE = 1;

#define MODE_WIFI  0
#define MODE_NOWIFI  1
bool modeWIFI = MODE_NOWIFI;
bool tryCbWIFI = MODE_NOWIFI;

//----------------------- Khai báo 1 số biến Blynk -----------------------
bool blynkConnect = true;
BlynkTimer timer; 

//---------------------- Nguyên mẫu hàm  ---------------------------------
void TaskBlynk(void *pvParameters);
void TaskMain(void *pvParameters);
void TaskButton(void *pvParameters);
void TaskSwitchAPtoSTA(void *pvParameters);
void TaskAutoWarning(void *pvParameters);
void TaskOLEDDisplay(void *pvParameters);
void TaskBuzzerWarning(void *pvParameters);
void measureDHT11();
void measureDustSensor();
String check_air_quality_and_send_to_blynk(bool autoWarning, bool sendBlynk, int temp, int humi, int dust);
//-------------------- Khai báo biến freeRTOS ----------------------------
TaskHandle_t TaskButton_handle      = NULL;
TaskHandle_t TaskOLEDDisplay_handle = NULL;

void setup() {  
  Serial.begin(115200);
  EEPROM.begin(512);

  //---------- Khai báp WiFi ---------
  Serial.println("Configuring access point...");
  WiFi.mode(WIFI_AP_STA);           //Both in Station and Access Point Mode
  // Khởi tạo nút nhấn
  pinMode(BUTTON_SET_PIN, INPUT_PULLUP);
  pinMode(BUTTON_UP_PIN, INPUT_PULLUP);
  pinMode(BUTTON_DOWN_PIN, INPUT_PULLUP);
  button_init(&buttonSET, BUTTON_SET_PIN, BUTTON1_ID);
  button_init(&buttonUP, BUTTON_UP_PIN, BUTTON2_ID);
  button_init(&buttonDOWN,   BUTTON_DOWN_PIN,   BUTTON3_ID);
  button_pressshort_set_callback((void *)button_press_short_callback);
  button_presslong_set_callback((void *)button_press_long_callback);
  // Khai báo cảm biến bụi
  dustSensor.begin();
  // Khởi tạo RELAY
  pinMode(RELAY, OUTPUT);
  digitalWrite(RELAY, ENABLE);
  // Khởi tạo BUZZER
  pinMode(BUZZER, OUTPUT);
  // Khởi tạo DHT11
  dht.begin();
  // Khởi tạo LED
  pinMode(LED, OUTPUT);
   // Khởi tạo OLED
  oled.begin(i2c_Address, true);
  oled.setTextSize(1);
  oled.setTextColor(SH110X_WHITE);
  oled.clearDisplay();
  oled.setCursor(20, 10);
  oled.print("HE THONG GIAM");
  oled.setCursor(15, 25);
  oled.print("SAT CHAT LUONG");
  oled.setCursor(30 , 40);
  oled.print("KHONG KHI");
  oled.display(); 
  delay(3000);
  // ---------- Kết nối WiFi ---------
  readEEPROM();
  connectSTA();
  // ---------- Đọc giá trị AutoWarning trong EEPROM ----------------
  autoWarning = EEPROM.read(200);
  Serial.print("Auto Warning : ");  Serial.println(autoWarning); 

  // ---------- Khai báo hàm FreeRTOS ---------
  xTaskCreatePinnedToCore(TaskOLEDDisplay,     "TaskOLEDDisplay" ,     1024*8 ,  NULL,  21 ,  &TaskOLEDDisplay_handle  , 1);
  xTaskCreatePinnedToCore(TaskDHT11,           "TaskDHT11" ,           1024*8 ,  NULL,  5  ,  NULL  , 1);
  xTaskCreatePinnedToCore(TaskDustSensor,      "TaskDustSensor" ,      1024*4 ,  NULL,  20 ,  NULL  , 1 );
  xTaskCreatePinnedToCore(TaskSwitchAPtoSTA,   "TaskSwitchAPtoSTA" ,   1024*4 ,  NULL,  5  ,  NULL  , 0);
  xTaskCreatePinnedToCore(TaskButton,          "TaskButton" ,          1024*4 ,  NULL,  5  ,  &TaskButton_handle ,  1);
  xTaskCreatePinnedToCore(TaskAutoWarning,     "TaskAutoWarning" ,     1024*4 ,  NULL,  5  ,  NULL ,  1);
  xTaskCreatePinnedToCore(TaskBuzzerWarning,   "TaskBuzzerWarning" ,   1024*4 ,  NULL,  5  ,  NULL ,  1);
  
  Blynk.virtualWrite(V4, autoWarning); 
}
void loop() { 
  vTaskDelete(NULL);
}

//------------------------------------------------------------------------------
//---------------------------Task Switch AP to STA -----------------------------

//---------------------------If IP is Hitted in Browser ------------------------
void D_AP_SER_Page() {
    int Tnetwork=0,i=0,len=0;
    String st="",s="";
    Tnetwork = WiFi.scanNetworks();//Scan for total networks available
    st = "<ul>";
    for (int i = 0; i < Tnetwork; ++i) {
      // Print SSID and RSSI for each network found
      st += "<li>";
      st +=i + 1;
      st += ": ";
      st += WiFi.SSID(i);
      st += " (";
      st += WiFi.RSSI(i);
      st += ")";
      st += (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*";
      st += "</li>";
    }
    st += "</ul>";
   IPAddress ip = WiFi.softAPIP();             //Get ESP IP Adress
        //String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
        s = "<ul>";
        s = "\n\r\n<!DOCTYPE HTML>\r\n<html><h1>GAS DETECTION SYSTEM</h1> ";
        //s += ipStr;
        s += "<p>";
        s += st;
        s += "<h1> Config Wifi and Token Blynk</h1>";
        s += "<form method='get' action='a'><label style='font-size:30px'>SSID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px'  name='ssid' length=32> <br> <br> <label style='font-size:30px'>Password:</label><input style='font-size: 30px; height:80px' name='pass' length=64> <br><br><label style='font-size:30px'>Tocken:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><input style='font-size: 30px; height:80px' name='token' length=64> <br><br><input  style='font-size:30px;height:60px; width: 100px' type='submit'></form>";
        s += "</ul>";
        s += "</html>\r\n\r\n";
      
    server.send( 200 , "text/html", s);
}
//--------------------------- Get SSID & Password  --------------------------- 
void Get_Req() {
  if (server.hasArg("ssid") && server.hasArg("pass") && server.hasArg("token")) {  
     sssid=server.arg("ssid");//Get SSID
     passs=server.arg("pass");//Get Password
     token=server.arg("token");//Get token
     Serial.println(sssid);
     Serial.println(passs);
     Serial.println(token);  
  }

  if(sssid.length()>1 && passs.length()>1 && token.length()>1) {
      ClearEeprom();//First Clear Eeprom
      delay(10);
      for (int i = 0; i < sssid.length(); ++i)
          EEPROM.write(i, sssid[i]);  
      for (int i = 0; i < passs.length(); ++i)
          EEPROM.write(32+i, passs[i]);
      for (int i = 0; i < token.length(); ++i)
          EEPROM.write(64+i, token[i]);
      
      EEPROM.commit();
          
      String s = "\r\n\r\n<!DOCTYPE HTML>\r\n<html><h1>Air Pollution Monitoring System</h1> ";
      s += "<p>Password Saved... Reset to boot into new wifi</html>\r\n\r\n";
      server.send(200,"text/html",s);
    }
    oled.clearDisplay();
    Serial.println(" RESTART ");

    vTaskSuspend(TaskOLEDDisplay_handle);
    oled.setCursor(0,15);
    oled.print(" RESTART ");
    oled.display();
    delay(2000);
    Serial.println("   DONE ");
    oled.setCursor(0,30);
    oled.print(" DONE ");
    oled.display();
    delay(2000);
    
    ESP.restart();
}

//--------------------------- connect STA mode and switch AP Mode if connect fail --------------------------- 
void connectSTA() {
      if ( Essid.length() > 1 ) {  
      Serial.println(Essid);        //Print SSID
      Serial.println(Epass);        //Print Password
      Serial.println(Etoken);        //Print token
      Etoken = Etoken.c_str();
      WiFi.begin(Essid.c_str(), Epass.c_str());   //c_str()
      int countConnect = 0;
      while (WiFi.status() != WL_CONNECTED) {
          delay(500);   
          if(countConnect++  == 20) {
            Serial.println("Ket noi Wifi that bai");
            Serial.println("Kiem tra SSID & PASS");
            Serial.println("Ket noi Wifi: ESP32 de cau hinh");
            Serial.println("IP: 192.168.4.1");

            
            break;
          }
      }
      Serial.println("");
      if(WiFi.status() == WL_CONNECTED) {
        Serial.println("Da ket noi Wifi: ");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP()); 
        Serial.println((char*)Essid.c_str());

        //vTaskSuspend(TaskOLEDDisplay_handle);
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Da ket noi Wifi: ");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print(Essid);
        oled.display(); 
        //vTaskResume(TaskOLEDDisplay_handle);

        strcpy(BLYNK_AUTH_TOKEN,Etoken.c_str());
        
        Blynk.config(BLYNK_AUTH_TOKEN);
        blynkConnect = Blynk.connect();
        if(blynkConnect == false) {
            Serial.println("Chua ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Chua ket noi BLYNK");
            oled.setCursor(0,36);
            oled.print("Kiem tra lai Token");
            oled.display(); delay(3000);

            switchAPMode(); 

            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("Ket noi Wifi: ESP32");
            oled.setCursor(0,24);
            oled.print("de cau hinh lai");
            oled.display(); delay(1000);
            oled.setCursor(0,36);
            oled.print("IP: 192.168.4.1");
            oled.display(); 

            digitalWrite(BUZZER, ENABLE);
            delay(2000);
            digitalWrite(BUZZER, DISABLE);
            delay(3000);
        }
        else {
            Serial.println("Da ket noi BLYNK");

            oled.setCursor(0,24);
            oled.print("Da ket noi BLYNK");
            oled.display(); delay(2000);
            xTaskCreatePinnedToCore(TaskBlynk,            "TaskBlynk" ,           1024*8 ,  NULL,  5  ,  NULL ,  0);
            timer.setInterval(1000L, myTimer);  
            buzzerBeep(5); 
            return; 
        }
       
        AP_STA_MODE = STA_MODE;
      }
      else {
        switchAPMode(); 
        oled.clearDisplay();
        oled.setCursor(0,0);
        oled.print("Ket noi Wifi that bai");
        oled.display(); delay(1000);
        oled.setCursor(0,12);
        oled.print("Kiem tra SSID & PASS");
        oled.display(); delay(1000);
        oled.setCursor(0,24);
        oled.print("Ket noi Wifi: ESP32");
        oled.setCursor(0,36);
        oled.print("de cau hinh");
        oled.display(); delay(1000);
        oled.setCursor(0,48);
        oled.print("IP: 192.168.4.1");
        oled.display(); 
        digitalWrite(BUZZER, ENABLE);
        delay(2000);
        digitalWrite(BUZZER, DISABLE);
        delay(3000);
      }
        
    }
}

//--------------------------- switch AP Mode --------------------------- 
void switchAPMode() {
  WiFi.softAP(ssid, pass);          //AP SSID and Password(Both in Ap and Sta Mode-According to Library)
  delay(100);                            //Stable AP
  server.on("/",D_AP_SER_Page);
  server.on("/a",Get_Req); 
  Serial.println("In Ap Mode");
  server.begin();  
  delay(300);
}
//--------------------------- Read Eeprom  ------------------------------------
void readEEPROM() {
    for (int i = 0; i < 32; ++i)     //Reading SSID
        Essid += char(EEPROM.read(i)); 
    for (int i = 32; i < 64; ++i)   //Reading Password
        Epass += char(EEPROM.read(i)); 
    for (int i = 64; i < 96; ++i)   //Reading Password
        Etoken += char(EEPROM.read(i)); 
}
//--------------------------- Clear Eeprom  ----------------------------------
void ClearEeprom() {
     Serial.println("Clearing Eeprom");
     for (int i = 0; i < 96; ++i) { EEPROM.write(i, 0); }
}

//---------------------------Task TaskSwitchAPtoSTA---------------------------
void TaskSwitchAPtoSTA(void *pvParameters) {
    while(1) {
        server.handleClient();   
        vTaskDelay(10/portTICK_PERIOD_MS);
    }
}

//-----------------------------------------------------------------------------
//---------------------------Task Blynk----------------------------------------

//----------------------- Send send Data value to Blynk every 2 seconds--------
void myTimer() {
    Blynk.virtualWrite(V0, tempValue);  
    Blynk.virtualWrite(V1, humiValue);
    Blynk.virtualWrite(V2, dustValue);
}

//--------------Read button from BLYNK and send notification back to Blynk-----------------------
int checkAirQuality = 0;
int virPin = 0;
BLYNK_WRITE(V3) {
    checkAirQuality = param.asInt();
    if(checkAirQuality == 1) {
      buzzerBeep(1);
      delay(500);
      check_air_quality_and_send_to_blynk(DISABLE, ENABLE, tempValue, humiValue, dustValue);
      virPin = 3;
    }
}

//------------------------- check autoWarning from BLYNK  -----------------------
BLYNK_WRITE(V4) {
    autoWarning = param.asInt();
    virPin = 4;
    buzzerBeep(1);
    EEPROM.write(200, autoWarning);  EEPROM.commit();
    delay(500);
}

//---------------------------Task TaskSwitchAPtoSTA---------------------------
void TaskBlynk(void *pvParameters) {
    while(1) {
      Blynk.run();
      timer.run(); 
      vTaskDelay(10/portTICK_PERIOD_MS);
    }
}
//-------------------------- Task OLED Display-------------------------------------------
void TaskOLEDDisplay(void *pvParameters)  {
  delay(15000);
 
  oled.clearDisplay();
  oled.display(); 
  while(1) {
    if(virPin == 3) {
      oled.clearDisplay();
      oled.setCursor(0,12);
      oled.print("Gui du lieu ");
      oled.setCursor(0,24);
      oled.print("den dien thoai");
      oled.display();
      virPin = 0;
      delay(1000);
    }
    else if(virPin == 4) {
      virPin = 0;
      if(autoWarning == 1) {
        Serial.println("Che do tu dong canh bao:BAT");

        oled.clearDisplay();
        oled.setCursor(0,12);
        oled.print("Che do tu dong ");
        oled.setCursor(0,24);
        oled.print("canh bao:BAT");
        oled.display();
      }
      else {
        Serial.println("Che do tu dong canh bao:TAT");

        oled.clearDisplay();
        oled.setCursor(0,12);
        oled.print("Che do tu dong ");
        oled.setCursor(0,24);
        oled.print("canh bao:TAT");
        oled.display();
      } 
      delay(1000);
    }
    oled.clearDisplay();
    oled.setCursor(0, 0);
    oled.print("Temp:");
    oled.print(tempValue);
    oled.print("*C");

    oled.setCursor(70, 0);
    oled.print("Humi:");
    oled.print(humiValue);
    oled.print("%");

    oled.setCursor(0, 12);
    oled.print("Dust PM2.5 : ");
    oled.print(dustValue);
    oled.print("ug/m3  ");

    int tempIndex = 0;
    int dustIndex = 0;
    int humiIndex = 0;
    
    if(tempValue < 20 )tempIndex = 1;
    else if(tempValue >= 20 && tempValue <  29)  tempIndex = 2;
    else if(tempValue >= 29 && tempValue <= 33)  tempIndex = 3;
    else tempIndex = 4;
    oled.setCursor(0, 24);
    oled.print(snOLEDTemp[tempIndex]);

    if(humiValue < 40 ) humiIndex = 1;
    else if(humiValue >= 40 && humiValue < 71)   humiIndex = 2;
    else humiIndex = 3;
    oled.setCursor(0, 36);
    oled.print(snOLEDHumi[humiIndex]);

    if(dustValue <= 12 ) dustIndex = 1;
    else if(dustValue > 12  && dustValue <= 35 ) dustIndex = 2;
    else if(dustValue > 35  && dustValue <= 55 ) dustIndex = 3;
    else if(dustValue > 55  && dustValue <= 150) dustIndex = 4;
    else if(dustValue > 150 && dustValue <= 250) dustIndex = 5;
    else dustIndex = 6;
    oled.setCursor(0, 48);
    oled.print(snOLEDDust[dustIndex]);
    
    oled.display();
    delay(500);
  }
}

//-------------------------- Task auto Warning-------------------------------------------
void TaskAutoWarning(void *pvParameters)  {
    delay(20000);
    while(1) {
      if(autoWarning == 1) {
          check_air_quality_and_send_to_blynk(ENABLE, ENABLE, tempValue, humiValue, dustValue);
      }
      delay(10000);
    }
}
//-------------------------- Task Buzze Warning------------------------------------------
/*
 * 
*/

void TaskBuzzerWarning(void *pvParameters)  {
    delay(20000);
    while(1) {
      int tempIndex = 0;
      int dustIndex = 0;
      int humiIndex = 0;
      
      if(tempValue < 20 )tempIndex = 1;
      else if(tempValue >= 20 && tempValue <  29)  tempIndex = 2;
      else if(tempValue >= 29 && tempValue <= 33)  tempIndex = 3;
      else tempIndex = 4;

      if(humiValue < 40 ) humiIndex = 1;
      else if(humiValue >= 40 && humiValue < 71)   humiIndex = 2;
      else humiIndex = 3;


      if(dustValue <= 12 ) dustIndex = 1;
      else if(dustValue > 12  && dustValue <= 35 ) dustIndex = 2;
      else if(dustValue > 35  && dustValue <= 55 ) dustIndex = 3;
      else if(dustValue > 55  && dustValue <= 150) dustIndex = 4;
      else if(dustValue > 150 && dustValue <= 250) dustIndex = 5;
      else dustIndex = 6;
      timeCountBuzzerWarning += 2;
      if(timeCountBuzzerWarning >= TIME_BUZZER_WARNING) {
        timeCountBuzzerWarning = 0;
        if(tempIndex == 1 || tempIndex == 4 || humiIndex == 1 || humiIndex == 4 || dustIndex >=4)
          buzzerBeep(3);
      }
      if(tempIndex == 1 || humiIndex == 1) blinkLED(2);
      else if( tempIndex == 4 ||  humiIndex == 4 || dustIndex >=4) blinkLED(3);
      else blinkLED(1);
      delay(2000);
    }
}

//--------------------------------Task TaskDHT11 -------------------------------
void TaskDHT11(void *pvParameters) { 
    //delay(10000);
    while(1) {
      int humi =  dht.readHumidity();
      int temp =  dht.readTemperature();
      if (isnan(humi) || isnan(temp) ) {
          Serial.println(F("Failed to read from DHT sensor!"));
      }
      else if(humi <= 100 && temp < 100) {
         // humiValue = humifilter.updateEstimate(humi);
         // tempValue = tempfilter.updateEstimate(temp);

          humiValue = humi;
          tempValue = temp;

          Serial.print(F("Humidity: "));
          Serial.print(humiValue);
          Serial.print(F("%  Temperature: "));
          Serial.print(tempValue);
          Serial.print(F("°C "));
          Serial.println();
      }
      delay(2000);
    }
}

//-----------------------Task Main Display and Control Device----------
void TaskDustSensor(void *pvParameters) {
    while(1) {
      dustValue = dustSensor.read();
      dustValue = dustValue - 20;
      if(dustValue <= 0)  dustValue = 0;
      dustValue = dustfilter.updateEstimate(dustValue);
      Serial.print("Dust Density = ");
      Serial.print(dustValue);
      Serial.println(" ug/m3");
      Blynk.virtualWrite(V2, dustValue); 
      delay(3000);
    }
}

//-----------------------Task Task Button ----------
void TaskButton(void *pvParameters) {
    while(1) {
      handle_button(&buttonSET);
      handle_button(&buttonUP);
      handle_button(&buttonDOWN);
      delay(10);
    }
}

// ---------------------- Hàm điều khiển còi -----------------------------
void buzzerBeep(int numberBeep) {
  for(int i = 0; i < numberBeep; ++i) {
    digitalWrite(BUZZER, ENABLE);
    delay(100);
    digitalWrite(BUZZER, DISABLE);
    delay(100);
  }  
}
// ---------------------- Hàm điều khiển LED -----------------------------
void blinkLED(int numberBlink) {
  for(int i = 0; i < numberBlink; ++i) {
    digitalWrite(LED, DISABLE);
    delay(300);
    digitalWrite(LED, ENABLE);
    delay(300);
  }  
}
void button_press_short_callback(uint8_t button_id) {
    switch(button_id) {
      case BUTTON1_ID :  
        buzzerBeep(1);
        Serial.println("btSET press short");
        break;
      case BUTTON2_ID :
        buzzerBeep(1);
        Serial.println("btUP press short");
        break;
      case BUTTON3_ID :
        buzzerBeep(1);
        Serial.println("btDOWN press short");
        check_air_quality_and_send_to_blynk(DISABLE, ENABLE, tempValue, humiValue, dustValue);
        //== Gui data len dien thoai
        break;  
    } 
} 

//-----------------Hàm xử lí nút nhấn giữ ----------------------
void button_press_long_callback(uint8_t button_id) {
    switch(button_id) {  
      case BUTTON1_ID :  
        buzzerBeep(2);
        Serial.println("btSET press long");
        AP_STA_MODE = 1 - AP_STA_MODE;
        switch(AP_STA_MODE) {
          case AP_MODE:
            Serial.println("WiFi AP Mode");
            Serial.println("Connect to WiFi:ESP32");
            Serial.println("192.168.4.1");
            switchAPMode();

            //== Cau hinh 192.168
            
            break;
          case STA_MODE:
            vTaskSuspend(TaskOLEDDisplay_handle);
            oled.clearDisplay();
            oled.setCursor(0,12);
            oled.print("RESTART");
            oled.display(); 
            delay(1000);
            ESP.restart();
            break;
        }
        break;
      case BUTTON2_ID :
        buzzerBeep(2);
        Serial.println("btUP press long");
        vTaskSuspend(TaskOLEDDisplay_handle);
        autoWarning = 1 - autoWarning;
        if(autoWarning == 1) {
          Serial.println("Che do tu dong canh bao:BAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:BAT");
          oled.display();
        }
        else {
          Serial.println("Che do tu dong canh bao:TAT");
          oled.clearDisplay();
          oled.setCursor(0,12);
          oled.print("Che do tu dong ");
          oled.setCursor(0,24);
          oled.print("canh bao:TAT");
          oled.display();
        } 
        Blynk.virtualWrite(V4, autoWarning); 
        EEPROM.write(200, autoWarning);EEPROM.commit();
        delay(2000);
        vTaskResume(TaskOLEDDisplay_handle);
        break;
      case BUTTON3_ID :
        buzzerBeep(2);
        Serial.println("btDOWN press long");
        break;  
 
   }   
}

//---------------------------------------------------------
/**
 * @brief Kiểm tra chất lượng không khí và gửi lên BLYNK
 *
 * @param autoWarning auto Warning
 * @param sendBlynk   Có gửi Blynk hay không
 * @param temp Nhiệt độ hiện tại    *C
 * @param humi Độ ẩm hiện tại        %
 * @param dust bụi PM2.5 hiện tại    ug/m3
 */
String check_air_quality_and_send_to_blynk(bool autoWarning, bool sendBlynk, int temp, int humi, int dust) {
  String notifications = "";
  String OLEDnotifications = "";
  int tempIndex = 0;
  int dustIndex = 0;
  int humiIndex = 0;
  if(autoWarning == 0) {
    if(temp < 20 )tempIndex = 1;
    else if(temp >= 20 && temp <  29)  tempIndex = 2;
    else if(temp >= 29 && temp <= 33)  tempIndex = 3;
    else tempIndex = 4;
    

    if(humi < 40 ) humiIndex = 1;
    else if(humi >= 40 && humi < 71)   humiIndex = 2;
    else humiIndex = 3;

    if(dust <= 12 ) dustIndex = 1;
    else if(dust > 12  && dust <= 35 ) dustIndex = 2;
    else if(dust > 35  && dust <= 55 ) dustIndex = 3;
    else if(dust > 55  && dust <= 150) dustIndex = 4;
    else if(dust > 150 && dust <= 250) dustIndex = 5;
    else dustIndex = 6;
    notifications = snTemp[tempIndex] + String(temp) + "*C . " + snHumi[humiIndex] + String(humi) + "% . " + snDust[dustIndex] + String(dust) + "ug/m3 . " ;
    OLEDnotifications = snOLEDTemp[tempIndex] + String(temp) + "*C . " + snOLEDHumi[humiIndex] + String(humi) + "% . " + snOLEDDust[dustIndex] + String(dust) + "ug/m3 . " ;  
    if(sendBlynk == ENABLE) Blynk.logEvent("check_data",notifications);
  }
  else {
    if(temp < 20 )tempIndex = 1;
    else if(temp >= 20 && temp <  29)  tempIndex = 0;
    else if(temp >= 29 && temp <= 33)  tempIndex = 0;
    else tempIndex = 4;
    
    if(humi < 40 ) humiIndex = 1;
    else if(humi >= 40 && humi < 71)   humiIndex = 0;
    else humiIndex = 3;

    if(dust <= 12 ) dustIndex = 0;
    else if(dust > 12  && dust <= 35 ) dustIndex = 0;
    else if(dust > 35  && dust <= 55 ) dustIndex = 0;
    else if(dust > 55  && dust <= 150) dustIndex = 4;
    else if(dust > 150 && dust <= 250) dustIndex = 5;
    else dustIndex = 6;
    if(tempIndex == 0 && humiIndex == 0 && dustIndex == 0)
      notifications = "";
    else {
      if(tempIndex != 0) notifications = notifications + snTemp[tempIndex] + String(temp) + "*C . ";
      if(humiIndex != 0) notifications = notifications + snHumi[humiIndex] + String(humi) + "% . " ;
      if(dustIndex != 0) notifications = notifications + snDust[dustIndex] + String(dust) + "ug/m3 . " ;
      if(sendBlynk == ENABLE)  Blynk.logEvent("auto_warning",notifications);
    }
  }
  Serial.println(notifications);
  return OLEDnotifications;
}
